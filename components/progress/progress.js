Component({
    options: {
      multipleSlots: true // 在组件定义时的选项中启用多slot支持
    },
    properties: {

      draw: {
        type: String,
        value: 'draw'
      },
      percent:{//进度条百分比
        type: String,
        value: '50'
      },
      r:{ //半径
        type: String,
        value: '50'
      },
      pw:{ //进度条宽度
        type: String,
        value: '50'
      },
      bgColor:{ //进度条宽度
        type: String,
        value: '#FFFFFF'
      },
      drawStartColor:{
        type: String,
        value: '#2661DD'
      },

      drawEndColor:{
        type: String,
        value: '#5956CC'
      }
    },

    data: {
        size: 0, //画板大小
    },
    methods: {
      /*
       * 有关参数
       * id : canvas 组件的唯一标识符 canvas-id
       * r : canvas 绘制圆形的半径
       * w : canvas 绘制圆环的宽度
       */
      drawCircleBg: function (el, r, w) {
        const ctx = wx.createCanvasContext(el, this);
            ctx.setLineWidth(w);// 设置圆环的宽度
            ctx.setStrokeStyle(this.data.bgColor); // 设置圆环的颜色
            ctx.setLineCap('round') // 设置圆环端点的形状
            ctx.beginPath();//开始一个新的路径
            ctx.arc(r, r, r - w, 0, 2 * Math.PI, false);
            //设置一个原点(110,110)，半径为100的圆的路径到当前路径
            ctx.stroke();//对当前路径进行描边
            ctx.draw();
      },
      drawCircle: function (el, r, w, step) {
        // 使用 wx.createContext 获取绘图上下文 context  绘制彩色进度条圆环
        const context = wx.createCanvasContext(el,this);
        let color=this.data.drawStartColor
        // 设置渐变
        // const gradient = context.createLinearGradient(0,0,2 * r, 0);
        // gradient.addColorStop(0, this.data.drawStartColor);
        // gradient.addColorStop(1, this.data.drawEndColor);
        if(step>=1){
          color=this.data.drawEndColor
        }
        context.setLineWidth(w);
        //context.setStrokeStyle(gradient);
        context.setStrokeStyle(color);
        context.setLineCap('round')
        context.beginPath();//开始一个新的路径
        // step 从0到2为一周
        context.arc(r, r, r - w, -Math.PI / 2, step * Math.PI - Math.PI / 2, false);
        context.stroke();//对当前路径进行描边
        context.draw()
      },
    },





  lifetimes: {
    //组件生命周期函数-在组件实例进入页面节点树时执行)
    attached: function () {
     const {windowWidth} = wx.getSystemInfoSync();//获取缴费器屏幕的宽度
      const el = this.data.draw; //画板元素
      const per = this.data.percent; //圆形进度
      const r = this.data.r; //圆形半径
      const standardWidth=750; //标准设计尺寸 750
      //获取屏幕宽度(并把真正的半径px转成rpx)
      let rpx = (windowWidth / 750) * r;
      //计算出画板大小
      this.setData({
        size: rpx * 2,
      });
      const w = this.data.pw*(windowWidth / 750);//圆形的宽度
      this.drawCircleBg(el+'-bg', rpx, w);//绘制 背景圆环
      let count=0;
      let timer=setInterval(()=>{
          if(count<per){
            this.drawCircle(el, rpx, w, (2 * count) / 100);//绘制 彩色圆环
            count++;
          }else{
            clearInterval(timer)
          }
      },10)
    },
  },
  })
