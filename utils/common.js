
var app = getApp();
var md5 = require("./md5.js");
//把对象按照属性名的字母顺序进行排列
function objKeySort(obj) {
  //先用Object内置类的keys方法获取要排序对象的属性名，再利用Array原型上的sort方法对获取的属性名进行排序，newkey是一个数组
  var newkey = Object.keys(obj).sort();
  var newObj = {};//创建一个新的对象，用于存放排好序的键值对
  for (var i = 0; i < newkey.length; i++) {//遍历newkey数组
    newObj[newkey[i]] = obj[newkey[i]];//向新创建的对象中按照排好的顺序依次增加键值对
  }
  return newObj;//返回排好序的新对象
}
var str = "";
//计算签名
function getSign(obj,apiSecret){
  var obj = objKeySort(obj);
  str = "";
  //将字符串拼接 key+value
  str = recursionString(obj);
  var sign_str = apiSecret + str;
  var sign = md5.md5(sign_str).toUpperCase();
  return sign;
}
//递归拼接参数
function recursionString(obj){
  for (var i in obj) {
    if (typeof (obj[i]) == 'object') {
      recursionString(obj[i]);
    } else {
      str += i + obj[i];
    }
  }
  return str;
}
module.exports = {
  getSign: getSign
}