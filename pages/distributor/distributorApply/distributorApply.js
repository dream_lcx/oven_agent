// pages/user/distributorApply/distributorApply.js
var util = require("../../../utils/util.js");
var mta = require('../../../utils/mta_analysis.js');
var app = getApp();
var user_id= '';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:{
      username: '',
      phone: '',
      id_card: '',
      address: '',
      referee_code: '',
    },
    phone:'',
    is_disabled: false,//推荐人账号能否修改
    referee_info:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    mta.Page.init();
    var pid = '', that = this, way = '';
    //二维码进入获取参数
    if (util.isExitsVariable(options.scene)) {
      var scene = util.sceneToArr(decodeURIComponent(options.scene));
      console.log(scene, "scne")
      if (util.isExitsVariable(scene.pid)) {
        pid = scene.pid;
        way = scene.way;
      }
    }
    if (util.isExitsVariable(options.pid)) {
      pid = options.pid;
      way = options.way;
    }
    if (pid != '') {
      that.getPidUserInfo(pid);//获取推荐人手机号
    }
    if (util.isExitsVariable(options.phone)) {
      var phone = options.phone;
      that.setData({
        phone:phone
      })
    }
    this.getApply();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },
    //获取申请详情
    getApply(){
      var that = this;
      if(that.data.phone==''){
        return false;
      }
      app.ajax({
        url: 'User/Spread/dealerApplyResult',
        method: "POST",
        data: { phone: that.data.phone },
        success: function (res) {
          wx.stopPullDownRefresh();
          if (res.data.code == 1000) {
            var info =  res.data.data;
            info.referee_code = info.referee_telphone;
            wx.setStorageSync('dealerApply', info);
            that.setData({
              info: res.data.data
            })
          } 
        }
      })
    },
  // 获取信息
  // getInfo() {
  //   var that = this;
  //   var info = wx.getStorageSync('dealerApply');
  //   if(info){
  //     that.setData({
  //       info
  //     })
  //   }
  // },
  //获取推荐人信息
  getPidUserInfo(pid){
    var that = this;
    app.ajax({
      url: 'User/User/verification_promotion',
      method: "POST",
      data: {
        user_id: pid,
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var temp = 'info.referee_code';
          that.setData({
            [temp]: res.data.data.workers_phone,
            is_disabled: true,
          })
        }
      }
    })
  },

  //输入框输入
  inputChange(e){
    var that = this;
    var name = e.currentTarget.dataset.name;
    var val = e.detail.value;
    var temp = 'info.' + name;
    that.setData({
      [temp]: val,
    })
    if(name == 'phone' && val.length==11){
      that.getParentInfo();
    }
  },

  // 申请
  formSubmit(e) {
    var that = this;
    var info = that.data.info;
    if (info.username == '') {
      app.showToast('请输入姓名');
      return
    }
    if (info.phone == '') {
      app.showToast('请输入手机号');
      return
    }
    if (info.id_card == '') {
      app.showToast('请输入身份证号');
      return
    }
    if (info.address == '') {
      app.showToast('请输入现居住地址');
      return
    }
    if (info.referee_code == '') {
      app.showToast('请输入推荐人账号或手机号');
      return
    }
    that.setData({
      isdisbale: true
    })

    //判断是否存在该推荐人、手机号、身份证号唯一性
    app.ajax({
      url: 'User/Spread/verificationExtension',
      method: "POST",
      data: {
        referee_code: info.referee_code,
        phone: info.phone,
        id_card:info.id_card
      },
      success: function (res) {
        if (res.data.code == 1000) {
          wx.setStorageSync('dealerApply', info);
          setTimeout(() => {
            that.setData({
              isdisbale: false
            })
            wx.navigateTo({
              url: '../uploadDocuments/uploadDocuments',
            })
          }, 500)
        }else{
          app.showToast(res.data.message);
          that.setData({
            isdisbale: false
          })
        }
      }
    })
  },
  //获取推荐人信息
  getParentInfo(){
    var that = this;
    app.ajax({
      url: 'User/User/getParentInfo',
      method: "POST",
      data: {
        telphone: that.data.info.phone,
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var temp = 'info.referee_code';
          that.setData({
            [temp]: res.data.data.telphone,
            referee_info:res.data.data,
            is_disabled:true
          })
        }else{
          var temp = 'info.referee_code';
          that.setData({
            [temp]: '',
            referee_info:'',
            is_disabled:false
          })
        }
      }
    })
  }
})