// pages/user/distributorApplyResult/distributorApplyResult.js
var app = getApp();
var img1 = app.globalData._network_path + 'review_waiting.png';
var img2 = app.globalData._network_path + 'review_success.png';
var img3 = app.globalData._network_path + 'review_fail.png';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: '',
    isshow: false,//是否显示申请结果
    img1: img1,
    img2: img2,
    img3: img3,

    keywords: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    if (options.phone){
      that.setData({
        isshow: true,
        phone: options.phone
      })
      that.getApply();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getApply();
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    wx.removeStorageSync('dealerApply');
  },

  //搜索输入
  searchInput(e) {
    this.setData({
      phone: e.detail.value
    })
  },
  //搜索结果
  search() {
    var that = this;
    that.setData({
      isshow: false,
      info: ''
    })
    that.getApply();
  },
  //获取申请详情
  getApply(){
    var that = this;
    app.ajax({
      url: 'User/Spread/dealerApplyResult',
      method: "POST",
      data: { phone: that.data.phone },
      success: function (res) {
        wx.stopPullDownRefresh();
        if (res.data.code == 1000) {
          that.setData({
            isshow: true,
            info: res.data.data
          })
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },
  //返回
  goBack(){
    wx.reLaunch({
      url: '/pages/extend/extendHome/extendHome',
    })
  },
  //重新申请
  againApply(){
    var that = this;
    var info = that.data.info;
    info.referee_code = info.referee_telphone;
    wx.setStorageSync('dealerApply', info);
    wx.navigateTo({
      url: '../distributorApply/distributorApply?phone='+info.phone,
    })
  },

  //预览图片
  previewImg(e){
    var that = this;
    var url = e.currentTarget.dataset.url;
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: [that.data.info.front_idcard_pic_url, that.data.info.back_idcard_pic_url] // 需要预览的图片http链接列表
    })
  },
  //签合同
  signContract(){
    wx.navigateTo({
      url: '/pages/extend/contract/contract',
    })
  }
})