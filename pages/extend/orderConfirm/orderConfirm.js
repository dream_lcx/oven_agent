// pages/shopping/orderConfirm/orderConfirm.js
var app = getApp();
var ch_latitude = "";
var ch_longitude = "";
var canDo=true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    server_list: [],//服务场景
    info: {
      shop_name: '',//店名
      contact_name: '',//联系人
      contact_phone: '',//联系电话
      valueName: [], //设置回显选择的地址(name)
      getAreaMsg: '', //选择的地址信息
      address: '',//详细地址
      server_index: '',//服务场景下标
      gas_fee: '',//燃气费用
      stoves_number_arr: [{
        name: '炒灶',
        name_desc: 'fry_num',
        num: 0,
      }, {
        name: '大锅灶',
        name_desc: 'big_pot_num',
        num: 0,
      }, {
        name: '矮汤炉',
        name_desc: 'short_soup_num',
        num: 0,
      }, {
        name: '其他炉具',
        name_desc: 'other_num',
        num: 0,
      }],//各炉具数量
      expect_stoves_number_arr: [{
        name: '炒灶',
        name_desc: 'fry_num',
        num: 0,
      }, {
        name: '大锅灶',
        name_desc: 'big_pot_num',
        num: 0,
      },
      //  {
      //   name: '矮汤炉',
      //   name_desc: 'short_soup_num',
      //   num: 0,
      // }
    ],//各炉具预计改造数量
      use_fan: '',//是否使用风机

      survey_info_arr: [{
        name: '炒灶',
        name_desc: 'fry',
        water: 5,
        gas: '',
        time: '',
      }, {
        name: '大锅灶',
        name_desc: 'big_pot',
        water: 10,
        num: '',
        gas: '',
        time: '',
      }, {
        name: '矮汤炉',
        name_desc: 'short_soup',
        water: 5,
        num: '',
        gas: '',
        time: '',
      }],//识别信息

      imgUrl: [],//显示用的
      img_url: [],//传给服务器用的
      imgUrl2: [],
      img_url2: [],
      imgUrl3: [],
      img_url3: [],
      videoUrl: '',//显示用的
      video_url: '',//传给服务器用的

      // 编辑
      id: '',
      explainer:'',//讲解人
      login_phone:''
    },

    addressShow: false,//是否打开地图
    chooseType: 1,//上传类型，1图片，2视频
    choosePosition: 1,//上传位置,1灶膛,2炉芯垂直90度拍摄,3操作面板45度拍摄
    explainers:[{"telphone":""}]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.headid = this.selectComponent("#addressid"); //引入地址
    that.appointmentConfiguration();//获取服务场景
    if (options.is_edit) { //编辑
      let tempInfo = 'info.id';
      that.setData({
        [tempInfo]: options.id
      })
      that.getDetail();// 获取详情
    } else {
      that.getStorg();//获取缓存
    }

  },
  /*** 生命周期函数--监听页面显示*/
  onShow: function () {
    canDo = true;
  },
  /*** 生命周期函数--监听页面卸载 重置全局变量我也不知道为什么要用全局变量*/
  onUnload: function () {
    ch_latitude = "";
    ch_longitude = "";
  },

  //获取缓存
  getStorg() {
    var that = this;
    var info = wx.getStorageSync('orderConfirm');
    if (info) {
      that.setData({
        info
      })
    }
  },
  // 获取详情
  getDetail() {
    var that = this;
    var id = that.data.info.id;
    app.ajax({
      url: 'User/Order/identification_info_details',
      method: "POST",
      data: { id: id },
      success: function (res) {
        wx.hideLoading();
        if (res.data.code == 1000) {
          var mydata = res.data.data;

          let areaId = [mydata.transform_province_code, mydata.transform_city_code, mydata.transform_area_code];
          let areaName = [mydata.transform_province_name, mydata.transform_city_name, mydata.transform_area_name];

          var server_index = ''; //服务场景
          that.data.server_list.forEach((item, index) => {
            if (mydata.service_scene == item.id) {
              server_index = index;
            }
          })

          //各炉具数量
          var stoves_number_arr = that.data.info.stoves_number_arr;
          stoves_number_arr[0].num = mydata.stoves_number[0].fry_num;
          stoves_number_arr[1].num = mydata.stoves_number[1].big_pot_num;
          stoves_number_arr[2].num = mydata.stoves_number[2].short_soup_num;
          stoves_number_arr[3].num = mydata.stoves_number[3].other_num;

          //各炉具预计改造数量
          var expect_stoves_number_arr = that.data.info.expect_stoves_number_arr;
          expect_stoves_number_arr[0].num = mydata.expect_transform_stoves_number[0].fry_num;
          expect_stoves_number_arr[1].num = mydata.expect_transform_stoves_number[1].big_pot_num;
          expect_stoves_number_arr[2].num = mydata.expect_transform_stoves_number[2].short_soup_num;

          //识别信息
          var survey_info_arr = that.data.info.survey_info_arr;
          survey_info_arr[0].gas = mydata.survey_info[0].gas;
          survey_info_arr[0].time = mydata.survey_info[0].time;
          survey_info_arr[1].gas = mydata.survey_info[1].gas;
          survey_info_arr[1].time = mydata.survey_info[1].time;
          survey_info_arr[2].gas = mydata.survey_info[2].gas;
          survey_info_arr[2].time = mydata.survey_info[2].time;

          var info = {
            id: mydata.id,
            shop_name: mydata.shop_name,
            contact_name: mydata.contact_name,
            contact_phone: mydata.contact_phone,
            getAreaMsg: { areaId, areaName },
            addressShow: false,
            address: mydata.transform_detailed_address,
            server_index: server_index,
            gas_fee: mydata.gas_fee,
            login_phone:mydata.login_phone,
            stoves_number_arr: stoves_number_arr,//各炉具数量
            expect_stoves_number_arr: expect_stoves_number_arr,//各炉具预计改造数量
            use_fan: mydata.use_fan,
            survey_info_arr: survey_info_arr,//识别信息
            imgUrl: mydata.environment_pic_url.position_1,//显示用的
            img_url: mydata.environment_pic.position_1,//传给服务器用的
            imgUrl2: mydata.environment_pic_url.position_2,
            img_url2: mydata.environment_pic.position_2,
            imgUrl3: mydata.environment_pic_url.position_3,
            img_url3: mydata.environment_pic.position_3,
            videoUrl: mydata.environment_video_url,//显示用的
            video_url: mydata.environment_video,//传给服务器用的
          };

          that.setData({
            info
          })
        }
      }
    })
  },
  //获取服务场景列表
  appointmentConfiguration() {
    var that = this;
    app.ajax({
      url: 'Common/Common/appointmentConfiguration',
      method: "POST",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            server_list: res.data.data.scene_type,
          })
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },

  //输入框输入
  inputChange(e) {
    var that = this;
    var name = e.currentTarget.dataset.name;
    let tempInfo = 'info.' + name;
    that.setData({
      [tempInfo]: e.detail.value
    })
  },

  //服务场景
  bindServerPicker(e) {
    var that = this;
    let tempInfo = 'info.server_index';
    that.setData({
      [tempInfo]: e.detail.value
    })
  },
  //选择户号
  bindCodePicker(e) {
    var that = this;
    let tempInfo = 'info.code_index';
    that.setData({
      [tempInfo]: e.detail.value
    })
  },
  //炉具数量输入
  inputStovesNumber(e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var temp = 'info.stoves_number_arr[' + index + '].num';
    that.setData({
      [temp]: e.detail.value
    })
  },
  //预计改造数量输入
  inputExpectStovesNumber(e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var temp = 'info.expect_stoves_number_arr[' + index + '].num';
    that.setData({
      [temp]: e.detail.value
    })
  },

  // 是否使用风机
  radioChangeUsefan(e) {
    var that = this;
    let tempInfo = 'info.use_fan';
    that.setData({
      [tempInfo]: e.detail.value
    })
  },

  //识别信息 燃气输入
  inputGas(e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var temp = 'info.survey_info_arr[' + index + '].gas';
    that.setData({
      [temp]: e.detail.value
    })
  },
  //识别信息 时间输入
  inputTime(e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var temp = 'info.survey_info_arr[' + index + '].time';
    that.setData({
      [temp]: e.detail.value
    })
  },

  //选择地址
  openAeraFun() {
    var that = this;
    wx.chooseLocation({
      latitude: ch_latitude,
      longitude: ch_longitude,
      success: function (res) {
        console.info(res);
        let tempInfo = 'info.address';
        that.setData({
          [tempInfo]: res.name
        })
        ch_latitude = res.latitude;
        ch_longitude = res.longitude;
      },
      fail: err => {
        console.log('数据',err.errMsg)
        if (err.errMsg === 'chooseLocation:fail auth deny') {
          wx.showModal({
            title: '提示',
            content: '打开地图,需要您授权位置信息',
            showCancel: false,
            success: data => {
              wx.openSetting({
                success(settingdata) {
                  if (settingdata.authSetting['scope.userLocation']) {
                    wx.showModal({
                      title: '提示',
                      content: '获取权限成功,再次点击将打开地图',
                      showCancel: false,
                    })
                  } else {
                    wx.showModal({
                      title: '提示',
                      content: '获取权限失败，无法打开地图~',
                      showCancel: false,
                    })
                  }
                },
              })
            }
          })
        }
      }
    })
    //获取地址授权

  },
  //打开地图选择
  openAddress() {
    var that = this;
    this.headid.startAddressAnimation(true);
    this.setData({
      addressShow: true
    })
  },
  //确定接受
  getSure(data) {
    var that = this;
    let [province_code, city_code, area_code] = data.detail.areaId;
    let [province, city, area] = data.detail.areaName;
    let tempInfo = 'info.getAreaMsg';
    that.setData({
      [tempInfo]: data.detail,
      addressShow: false
    })
  },
  cancel_fun() {
    this.setData({
      addressShow: false
    })
  },



  // 取消按钮
  cancelFun() {
    wx.showModal({
      title: '是否返回上一页面?',
      content: '当前数据不会保存',
      confirmColor: '#DF0101',
      success(res) {
        if (res.confirm) {
          wx.navigateBack({
            delta: 1
          })
        } else if (res.cancel) {

        }

      }
    })
  },


  // 添加图片 打开中间页
  openChoose(e) {
    var that = this;
    that.setData({
      isShow: true,
      chooseType: e.currentTarget.dataset.choose,//上传类型,1照片,2视频
      choosePosition: e.currentTarget.dataset.position,//上传位置,1灶膛,2炉芯垂直90度拍摄,3操作面板45度拍摄
    })
  },
  //关闭中间页(关闭)
  closeShoot() {
    var that = this;
    that.setData({
      isShow: false,
    })
  },
  //打开相机
  openAbout(e) {
    var that = this;
    var type = e.currentTarget.dataset.type;
    //关闭中间页面 打开自定义照相机
    that.setData({
      isShow: false,
    })
    
    var sourceType = '';
    if (type == 1) {
      sourceType = 'album';
    } else if (type == 2) {
      sourceType = 'camera';
    }
    var count = 3;
    var length = 0;
    if (that.data.chooseType == 1) {//图片
      //炉膛
      if (that.data.choosePosition == 1) {
        length = that.data.info.imgUrl.length || 0;
        count = 3 - length;
      }
      //炉芯垂直90度拍摄
      if (that.data.choosePosition == 2) {
        length = that.data.info.imgUrl2.length || 0;
        count = 3 - length;
      }
      //操作面板45度拍摄
      if (that.data.choosePosition == 3) {
        length = that.data.info.imgUrl3.length || 0;
        count = 3 - length;
      }
      wx.chooseImage({
        count: count,
        sizeType: ['compressed'],
        sourceType: [sourceType],
        success: function (res) {
          const tempFilePaths = res.tempFilePaths;
          wx.showLoading({
            mask: true,
            title: '上传中',
          })
          that.uploadImageArr(tempFilePaths);
        },
      })
    } else {//视频
      wx.chooseVideo({
        sourceType: [sourceType],
        compressed: true,
        maxDuration: 60,
        camera: 'back',
        success: function (res) {
          if (res.duration > 60) {
            app.showToast('视频时长必须小于60秒');
            return
          }
          const tempFilePath = res.tempFilePath;
          wx.showLoading({
            mask: true,
            title: '上传中',
          })
          that.uploadImageArr([tempFilePath]);
        },
      })
    }
  },
  // 上传环境图 -多图
  uploadImageArr(tempFilePaths) {
    console.log('获取到的数据你好', tempFilePaths)
    var that = this;
    for (var i in tempFilePaths) {
      wx.uploadFile({
        url: app.globalData._url + 'Common/Common/upload',
        filePath: tempFilePaths[i],
        name: 'file',
        success: function (result) {
          if (result.statusCode !== 200) {
            wx.hideLoading();
            app.showToast('上传失败,请重新上传');
            return
          }
          var data = JSON.parse(result.data);
          if (data.code == 1000) {
            if (that.data.chooseType == 1) {//图片
              //炉膛
              if (that.data.choosePosition == 1) {
                var imgUrl = that.data.info.imgUrl;
                var img_url = that.data.info.img_url;
                imgUrl.push(data.data.all_url);
                img_url.push(data.data.url);
                let tempInfo = 'info.imgUrl';
                let tempInfo2 = 'info.img_url';
                that.setData({
                  [tempInfo]: imgUrl,
                  [tempInfo2]: img_url
                })
              }
              //炉芯垂直90度拍摄
              if (that.data.choosePosition == 2) {
                var imgUrl2 = that.data.info.imgUrl2;
                var img_url2 = that.data.info.img_url2;
                imgUrl2.push(data.data.all_url);
                img_url2.push(data.data.url);
                let tempInfo = 'info.imgUrl2';
                let tempInfo2 = 'info.img_url2';
                that.setData({
                  [tempInfo]: imgUrl2,
                  [tempInfo2]: img_url2
                })
              }
              //操作面板45度拍摄
              if (that.data.choosePosition == 3) {
                var imgUrl3 = that.data.info.imgUrl3;
                var img_url3 = that.data.info.img_url3;
                imgUrl3.push(data.data.all_url);
                img_url3.push(data.data.url);
                let tempInfo = 'info.imgUrl3';
                let tempInfo2 = 'info.img_url3';
                that.setData({
                  [tempInfo]: imgUrl3,
                  [tempInfo2]: img_url3
                })
              }
              wx.hideLoading();
            } else {//视频
              var videoUrl = data.data.all_url;
              var video_url = data.data.url;
              let tempInfo = 'info.videoUrl';
              let tempInfo2 = 'info.video_url';
              that.setData({
                [tempInfo]: videoUrl,
                [tempInfo2]: video_url
              })
              wx.hideLoading();
            }
          } else {
            wx.hideLoading();
            app.showToast('上传失败,请重新上传');
          }
        },
        fail: function (fail) {
          wx.hideLoading();
          app.showToast('上传失败,请重新上传');
        }
      })
    }
  },
  // 图片放大预览
  previewImage(e) {
    wx.previewImage({
      current: e.currentTarget.dataset.url,
      urls: this.data.info.imgUrl // 需要预览的图片http链接列表
    })
  },
  // 删除图片
  reduceImg(e) {
    var index = e.currentTarget.dataset.index;
    var position = e.currentTarget.dataset.position;
    if (position == 1) {
      var imgUrl = this.data.info.imgUrl;
      var img_url = this.data.info.img_url;
      imgUrl.splice(index, 1);
      img_url.splice(index, 1);
      let tempInfo = 'info.imgUrl';
      let tempInfo2 = 'info.img_url';
      this.setData({
        [tempInfo]: imgUrl,
        [tempInfo2]: img_url
      })
    }
    if (position == 2) {
      var imgUrl2 = this.data.info.imgUrl2;
      var img_url2 = this.data.info.img_url2;
      imgUrl2.splice(index, 1);
      img_url2.splice(index, 1);
      let tempInfo = 'info.imgUrl2';
      let tempInfo2 = 'info.img_url2';
      this.setData({
        [tempInfo]: imgUrl2,
        [tempInfo2]: img_url2
      })
    }
    if (position == 3) {
      var imgUrl3 = this.data.info.imgUrl3;
      var img_url3 = this.data.info.img_url3;
      imgUrl3.splice(index, 1);
      img_url3.splice(index, 1);
      let tempInfo = 'info.imgUrl3';
      let tempInfo2 = 'info.img_url3';
      this.setData({
        [tempInfo]: imgUrl3,
        [tempInfo2]: img_url3
      })
    }
  },
  //删除视频
  reduceVideo() {
    let tempInfo = 'info.videoUrl';
    let tempInfo2 = 'info.video_url';
    this.setData({
      [tempInfo]: '',
      [tempInfo2]: ''
    })
  },

  //提交表单
  formSubmit(e) {
    let that = this;
    let formVal = e.detail.value;
    let [province_code, city_code, area_code] = that.data.info.getAreaMsg.areaId ? that.data.info.getAreaMsg.areaId : '';
    let [province, city, area] = that.data.info.getAreaMsg.areaName ? that.data.info.getAreaMsg.areaName : '';
    let numreg = /^[1][3-9][\d]{9}$/; //手机号码正则
    if (formVal.shop_name == '') {
      app.showToast('请输入店名', "none", 2000, function () { });
      return false;
    }
    if (formVal.login_phone == '') {
      app.showToast('登录手机号不能为空', "none", 2000, function () { });
      return false;
    }
    if (numreg.test(formVal.login_phone) == false || formVal.login_phone.length < 11) {
      app.showToast('请输入正确的登录手机号', "none", 2000, function () { });
      return false;
    }
    if (formVal.contact_name == '') {
      app.showToast('联系人不能为空', "none", 2000, function () { });
      return false;
    }
    if (formVal.contact_phone == '') {
      app.showToast('联系电话不能为空', "none", 2000, function () { });
      return false;
    }
    if (numreg.test(formVal.contact_phone) == false || formVal.contact_phone.length < 11) {
      app.showToast('请输入正确的联系电话', "none", 2000, function () { });
      return false;
    }
    if (that.data.info.getAreaMsg == '') {
      app.showToast('请选择省市区', "none", 2000, function () { });
      return false;
    }
    if (that.data.info.address == '') {
      app.showToast('请输入改造详细地址', "none", 2000, function () { });
      return false;
    }
    that.data.explainers.forEach((item,index)=>{
      if (numreg.test(item.telphone) == false || item.telphone.length < 11) {
        app.showToast('请输入正确的讲解人电话', "none", 2000, function () { });
        return false;
      }
    })
    
    if (that.data.info.use_fan == '') {
      app.showToast('请选择现有炉具燃烧器是否都使用风机', "none", 2000, function () { });
      return false;
    }
    let stoves_number = []; //炉具数量
    that.data.info.stoves_number_arr.forEach((item, index) => {
      let key = item.name_desc;
      let val = item.num || 0;
      let obj = { [key]: val };
      stoves_number.push(obj);
    })
    let expect_transform_stoves_number = []; //预计改造数量
    that.data.info.expect_stoves_number_arr.forEach((item, index) => {
      let key = item.name_desc;
      let val = item.num || 0;
      let obj = { [key]: val };
      expect_transform_stoves_number.push(obj);
    })
    let survey_info = []; //识别信息
    that.data.info.survey_info_arr.forEach((item, index) => {
      let key = item.name_desc;
      let obj = { [key]: key, gas: item.gas, time: item.time };
      survey_info.push(obj);
    })
    if (that.data.info.img_url.length <= 0) {
      app.showToast('请上传炉膛照片', "none", 2000, function () { });
      return false;
    }
    if (that.data.info.img_url2.length <= 0) {
      app.showToast('请上传炉芯垂直90度拍摄照片', "none", 2000, function () { });
      return false;
    }
    if (that.data.info.img_url3.length <= 0) {
      app.showToast('请上传操作面板45度拍摄照片', "none", 2000, function () { });
      return false;
    }
    if (that.data.info.video_url == '') {
      app.showToast('请上传环境视频', "none", 2000, function () { });
      return false;
    }
    var environment_pic = {};//环境照片
    environment_pic.position_1 = that.data.info.img_url;
    environment_pic.position_2 = that.data.info.img_url2;
    environment_pic.position_3 = that.data.info.img_url3;
    let updata = {
      id: that.data.info.id,
      shop_name: formVal.shop_name,
      contact_name: formVal.contact_name,
      contact_phone: formVal.contact_phone,
      province,
      province_code,
      city,
      city_code,
      area,
      area_code,
      address: formVal.address,
      // scene_type: that.data.server_list[that.data.info.server_index].id || '',//服务场景
      gas_fee: formVal.gas_fee,//每月燃气费用
      stoves_number,//炉具数量
      expect_transform_stoves_number,//预计改造数量
      use_fan: that.data.info.use_fan,//是否使用风机
      survey_info,//识别信息
      environment_pic,//环境照片
      environment_video: that.data.info.video_url,//环境视频
      explainer:that.data.explainers,
      login_phone:that.data.info.login_phone
    }
    if (!canDo) {
      return false
    }
    canDo = false;
    app.ajax({
      url: 'User/Order/addOrder',
      data: updata,
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast('报单成功');
          wx.removeStorageSync('orderConfirm');
          setTimeout(() => {
            wx.redirectTo({
              url: '../myOrderList/myOrderList',
            });
          }, 1000)
        } else {
          app.showToast(res.data.message, "none", 2000, function () { });
        }
        setTimeout(() => {
          canDo = true;
        }, 1000)
      }
    })
  },

  //缓存
  saveData() {
    var that = this;
    wx.setStorageSync('orderConfirm', that.data.info);
    setTimeout(() => {
      app.showToast('保存成功');
    }, 500)
  },
  //锅具输入框获取焦点
  stovesleave(e){
    var that = this;
    var value = e.detail.value;
    var index = e.currentTarget.dataset.index;
    var str = "info.stoves_number_arr[" + index + "].num"
    if(value == 0){
      that.setData({
      //更新页面input框显示
        [str]: ""
      })
    }
  },
  //锅具输入框失去焦点
  stovesblur(e){
    var that = this;
    var value = e.detail.value;
    var index = e.currentTarget.dataset.index;
    var str = "info.stoves_number_arr[" + index + "].num"
    if(value == ""){
      that.setData({
      //更新页面input框显示
        [str]: 0
      })
    }
  },
  //预计改造输入框获取焦点
  expectleave(e){
    var that = this;
    var value = e.detail.value;
    var index = e.currentTarget.dataset.index;
    var str = "info.expect_stoves_number_arr[" + index + "].num"
    if(value == 0){
      that.setData({
      //更新页面input框显示
        [str]: ""
      })
    }
  },
  //预计改造输入框失去焦点
  expectblur(e){
    var that = this;
    var value = e.detail.value;
    var index = e.currentTarget.dataset.index;
    var str = "info.expect_stoves_number_arr[" + index + "].num"
    if(value == ""){
      that.setData({
      //更新页面input框显示
        [str]: 0
      })
    }
  },
  //添加讲解人
  addExplainer(){
    let explainers = this.data.explainers;
    let obj = {'telphone':''};
    explainers.push(obj);
    this.setData({ explainers })
  },
  //输入讲解人
  inputExPlainer(e){
    let { index,name } = e.currentTarget.dataset;
    let obj = 'explainers['+ index +'].'+name;
    this.setData({ 
      [obj]: e.detail.value
    })
  },
  //删除讲解人
  delExplainer(e){
    let { index } = e.currentTarget.dataset;
    let explainers = this.data.explainers;
    explainers.splice(index, 1);
    this.setData({ explainers })
    console.log(this.data.explainers,"e")
  }
 
})
