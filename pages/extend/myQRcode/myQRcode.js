// pages/user/myQRcode/myQRcode.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    userInfo:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getUserInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },


  // 获取用户信息
  getUserInfo() {
    var that = this;
    app.ajax({
      url: 'User/Spread/getInfo',
      method: "POST",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            userInfo: res.data.data
          })
          that.getPageQr();
        }
      }
    })
  },
  // 获取推广码
  getPageQr() {
    var that = this;
    let u_id = that.data.userInfo.user_id;
    app.ajax({
      url: 'Common/Wx/getPageQr',
      method: "POST",
      data: {
        query: "pid=" + u_id + "&way=2",
        page: 'pages/distributor/distributorApply/distributorApply',
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            code_url: res.data.data
          })
        }
      }
    })
  },
})