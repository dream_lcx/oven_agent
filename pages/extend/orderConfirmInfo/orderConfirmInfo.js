// pages/order/orderConfirmInfo/orderConfirmInfo.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: "",//识别id
    type: 1,//1、测试确认单、2保管清单、3分享确认单
    work_order_type: 1,//1试用2改造

    info: '',
    is_sign: false,//是否确认完了，是否能签字
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var id = options.id||'';
    var type = options.type || 1;
    var work_order_type = options.work_order_type || 1;
    that.setData({
      id,
      type,
      work_order_type,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.getInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    that.getInfo();
  },

  //获取确认单详情
  getInfo() {
    var that = this;
    app.ajax({
      url: 'User/Work/getEnergySavingData',
      method: "POST",
      data: {
        identification_id: that.data.id,
        type: that.data.type,
        work_order_type: that.data.work_order_type,
      },
      success: function (res) {
        wx.stopPullDownRefresh();
        if (res.data.code == 1000) {
          var data = res.data.data;
          that.setData({
            info: data
          })
          
        }
      }
    })
  },

  // 查看大图
  previewImage(e) {
    var currentImg = e.currentTarget.dataset.url;
    wx.previewImage({
      current: currentImg, // 当前显示图片的http链接
      urls: [currentImg] // 需要预览的图片http链接列表
    })
  },
})