// pages/user/collectCoupon/collectCoupon.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',//优惠券id
    user_id: '',//用户id

    userInfo: '',//推荐人信息

    couponInfo: '',//优惠券详情
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(123, options)
    this.setData({
      id: options.id,
      user_id: options.user_id
    })
    this.getUserInfo();
    this.getCouponInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  //获取推荐人信息
  getUserInfo() {
    var that = this;
    app.ajax({
      url: 'User/User/getUserInfo',
      method: "POST",
      data: {
        user_id: that.data.user_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            userInfo: res.data.data
          })
        }
      }
    })
  },
  //获取优惠券详情
  getCouponInfo() {
    var that = this;
    app.ajax({
      url: 'User/Coupon/getCouponInfo',
      method: "POST",
      data: {
        id: that.data.id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            couponInfo: res.data.data
          })
        }
      }
    })
  },
  //领取优惠券
  collectBtn(){
    var that = this;
    app.ajax({
      url: 'User/Coupon/receiveCoupon',
      method: "POST",
      data: {
        id: that.data.id,
        user_id: that.data.user_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.message, "none", 2000, function () {
            wx.redirectTo({
              url: '/pages/user/coupon/coupon'
            })
          });
        }else{
          app.showToast(res.data.message);
        }
      }
    })
  },
})