// pages/user/myOrderList/myOrderList.js
var app=getApp();
var _PAGESIZE = 10;//默认页量
var _PAGE = 1;//默认页码
var cancelsOrder = false;//取消
var sureOrder = false;//确认收货
var load= true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    client: '', //1用户端5经销商端,默认用户端
    keywords: '',
    list:[],//订单列表
    
    hasMore: false, //数据是否加载完成
    loading: false,
    load_show: false,//整体加一个开关 加载

    progressShow:false,//进度弹窗显示
    isorder: false,//是否是工单列表
    id:'',
    progressData: '',
    currIndex: -1,//打开更多盒子 默认不打开Pop
    isMaskShow: false,//遮罩层显隐
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that=this;
    _PAGE = 1;
    that.getList();
    that.setData({
      currIndex: -1
    });
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    that.setData({
      loading: true
    })
    _PAGE = 1;
    that.getList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    _PAGE++;
    that.getList();
  },


  //搜索输入
  searchInput(e){
    this.setData({
      keywords: e.detail.value
    })
  },
  //搜索订单
  searchOrder(){
    _PAGE = 1;
    this.getList();
  },
  //取消订单
  cancelOrder(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '是否取消该订单',
      confirmColor: '#c80000',
      success: function (res) {
        if (res.confirm) {
          if(cancelsOrder){
            return false
          }
          cancelsOrder=true;
          app.ajax({
            url: 'User/Order/cancelIdentificationTable',
            method: 'POST',
            data:{
              identification_id: id
            },
            success: function(res){
              app.showToast(res.data.message);
              if(res.data.code ==1000){
                var timer = setTimeout(function(){
                  that.getList();                 
                },1000)
              }
              cancelsOrder=false;
            }
          })
                      
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  // 查看进度
  lookProgress(e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    that.setData({
      progressShow: true,
      progressData: [],
      id: id
    })
    app.ajax({
      url: 'User/Work/projectProgress',
      method: 'POST',
      data: {
        identification_id: id,
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            progressData: res.data.data
          })
        }
      }
    })
  },
  // 关闭进度弹窗
  cancelModal: function () {
    var that = this;
    that.setData({
      progressShow: false,
      id: ''
    })
  },
  // 打开更多弹窗
  moreShow(e) {
    let { index } = e.currentTarget.dataset;
    if (index === this.data.currIndex) {
      this.setData({ 
        currIndex: -1,
        isMaskShow: false,
      })
    } else {
      this.setData({ 
        currIndex: index,
        isMaskShow: true,
      })
    }
  },
  // 阻止遮罩滚动
  myCatchTouch: function () {
    return;
  },
  // 关闭遮罩弹窗
  closeMask() {
    var that = this;
    that.setData({
      currIndex: -1,
      isMaskShow: false,
    })
  },
  // 跳转上传合同
  uploadContract(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/subpackage/pages/extend/uploadContract/uploadContract?id=' + id,
    })
  },
  // 跳转查看合同
  lookContract(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/subpackage/pages/extend/contractList/contractList?id=' + id,
    })
  },
  // 跳转订单详情
  goDetail(e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../myOrderDetail/myOrderDetail?id=' + id,
    })
  },
  // 已拒绝单重新编辑
  editOrder(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../orderConfirm/orderConfirm?id=' + id + '&is_edit=true',
    })
  },
  //查看合同
  orderContract(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../contractList/contractList?id=' + id,
    })
  },

  //获取我的订单列表
  getList(){
    var that = this;
    var _url = 'User/Order/identification_info_list';
    if (that.data.isorder){
      _url = 'User/Work/getWorkList';
    }
    app.ajax({
      url: _url,
      method: "POST",
      load: load,
      msg: '加载中...',
      data: { page: _PAGE, pageSize: _PAGESIZE, keyword: that.data.keywords, },
      success: function (res) {
        wx.hideLoading();
        wx.stopPullDownRefresh();
        if (res.data.code == 1000) {
          if (_PAGE == 1) {
            that.setData({
              list: res.data.data,
              empty: false
            });
          } else {
            that.setData({
              list: that.data.list.concat(res.data.data),
              empty: false
            });
          }
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          load= false;
        }  else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          load_show: true
        })
        
      }
    })
  },

})