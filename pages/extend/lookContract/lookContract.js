// pages/subpackage/pages/extend/lookContract/lookContract.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',//识别id
    info: '',//详情
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      id: options.id || '',
    })
    that.getInfo();//获取合同信息
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  //获取详情
  getInfo() {
    var that = this;
    app.ajax({
      url: 'User/Contract/contractEnclosure',
      method: "POST",
      data: { identification_id: that.data.id },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            info: res.data.data
          })
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },

  // 图片放大预览
  previewImage(e) {
    var that = this;
    var data = that.data.info;
    var urls = [];
    data.forEach((item) => {
      urls.push(item.new_name_url);
    })
    wx.previewImage({
      current: e.currentTarget.dataset.url,
      urls: urls // 需要预览的图片http链接列表
    })
  },
})