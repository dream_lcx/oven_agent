// pages/user/spreadRecord/spreadRecord.js
var app = getApp();
var load = true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],//业绩列表
    page: 1,//当前页
    pagesize: 10,//每页条数
    hasMore: false, //数据是否加载完成
    loading: false,
    load_show: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    this.setData({
      page:1,
      key:that.data.searchKey
    })
    that.getSpreadIndex();
  },



  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      list: [],//合同列表
      load_show: false,
      page: 1,
    })
    this.getSpreadIndex();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    var page = that.data.page + 1;
    that.setData({
      page: page,
      loading: true
    })
    that.getSpreadIndex();
  },
  // 输入搜索字段
  inputSearch: function (e) {
    var that = this;
    var searchKey = e.detail.value;
    that.setData({
      searchKey: searchKey
    });
  },
  //点击搜索图标搜索
  searchOrder2: function () {
    var that = this;
    var key = that.data.searchKey;
    // if (key == '' || key == null) {
    //   app.showToast("请输入搜索关键字", "none", 2000, function () { });
    //   return;
    // }
    that.setData({
      page:1,
      list:[],
      hasMore: false,
      loading: false,
      load_show: false,
    })
    that.getSpreadIndex();
  },
  //获取我的业绩列表
  getSpreadIndex() {
    var that = this;
    var page = that.data.page;
    var pagesize = that.data.pagesize;
    var key = that.data.searchKey;
    app.ajax({
      url: 'User/Spread/index',
      method: "POST",
      load: load,
      msg: '加载中...',
      data: {
        page: page,
        pagesize: pagesize,
        keywords: key
      },
      success: function (res) {      
        wx.stopPullDownRefresh();
        if (res.data.code == 1000) {
          if (page == 1) {
            that.setData({
              list: res.data.data.data
            })
          } else {
            that.setData({
              list: that.data.list.concat(res.data.data.data)
            })
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.data.length < pagesize) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          load = false;
        } else {
          if (page == 1) {
            that.setData({
              hasMore: false,
              loading: false
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false
            })
          }
        }
        that.setData({
          load_show: true
        })
      }
    })
  },

})