// pages/user/myOrderDetail/myOrderDetail.js
var app=getApp();
var sureOrder=false;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',//识别id
    client: 1,//1用户,5推广
    orderDetail:'',//订单详细信息
    status: '',//审核态状态 1待审核2审核通过3审核失败
    realnameShow: false,//弹窗显示

    moreShowM1: false,//风机功率 显示更多
    moreShowM2: false,//测量信息 显示更多
    moreShow: false,//炉具信息 显示更多
    moreProgress: false,//跟多进度
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      id: options.id,
      client: options.client||1,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that=this;
    wx.showLoading({
      title: '加载中...',
    })
    that.getOrderDetail();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  // 关闭弹窗
  cancelModal: function () {
    var that = this;
    that.setData({
      realnameShow: false,
      id: ''
    })
  },

  // 取消订单
  cancelOrder(){
    var that = this;
    var id = that.data.id;
    wx.showModal({
      title: '提示',
      content: '是否取消该订单',
      confirmColor: '#c80000',
      success: function (res) {
        if (res.confirm) {
          app.ajax({
            url: 'User/Order/cancelIdentificationTable',
            method: 'POST',
            data: {
              identification_id: id
            },
            success: function (res) {
              app.showToast(res.data.message);
              var timer = setTimeout(function(){
                wx.navigateBack({
                  delta:1
                })
              },1000);
            }
          })

        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  // 获取订单详情
  getOrderDetail(){
    var that=this;
    var id = that.data.id;
    app.ajax({
      url: 'User/Order/identification_info_details',
      method: "POST",
      data: { id: id },
      success: function (res) {
        wx.hideLoading();
        if (res.data.code == 1000){
          that.setData({
            orderDetail:res.data.data,
          })
        }
      }
    })
  },
  // 风机功率 显示更多
  lookMoreM1(){
    this.setData({
      moreShowM1: !this.data.moreShowM1
    })
  },
  // 测量信息 显示更多
  lookMoreM2() {
    this.setData({
      moreShowM2: !this.data.moreShowM2
    })
  },
  // 炉具信息 显示更多
  lookMore() {
    this.setData({
      moreShow: !this.data.moreShow
    })
  },
  // 进度 查看更多
  lookmoreProgress(){
    this.setData({
      moreProgress: !this.data.moreProgress
    })
  },
  // 查看大图
  previewImage(e){
    var currentImg = e.currentTarget.dataset.url;
    wx.previewImage({
      current: currentImg, // 当前显示图片的http链接
      urls: this.data.orderDetail.environment_pic_url.position_1 // 需要预览的图片http链接列表
    })
  },
  previewImage2(e) {
    var currentImg = e.currentTarget.dataset.url;
    wx.previewImage({
      current: currentImg, // 当前显示图片的http链接
      urls: this.data.orderDetail.environment_pic_url.position_2 // 需要预览的图片http链接列表
    })
  },
  previewImage3(e) {
    var currentImg = e.currentTarget.dataset.url;
    wx.previewImage({
      current: currentImg, // 当前显示图片的http链接
      urls: this.data.orderDetail.environment_pic_url.position_3 // 需要预览的图片http链接列表
    })
  },
// 已拒绝单重新编辑
editOrder(){
  var that = this;
  wx.navigateTo({
    url: '../orderConfirm/orderConfirm?id=' + that.data.id + '&is_edit=true',
  })
},
})
