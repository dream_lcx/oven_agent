// pages/user/inviteHome/inviteHome.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bg_img: app.globalData._network_path + 'extend-invitebg.png',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  // 邀请记录
  myInvite() {
    wx.navigateTo({
      url: '/pages/user/myInvite/myInvite',
    })
  },
  // 分享
  promotionFriends() {
    var that = this;
    wx.navigateTo({
      url: '/pages/marketing/marketing',
    })
  },
})