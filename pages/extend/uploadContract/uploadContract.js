// pages/subpackage/pages/extend/uploadContract/uploadContract.js
var app = getApp();
var myData = new Date();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    order_id: '',
    fileUrl: [],//全路径显示用的
    file_url: [],//半路径传给服务器的

    contractInfo: '',//合同信息
    typeList: [],//合同类型列表
    type_index: '',//选择的合同类型下标

    today_data: myData.getFullYear() + '-' + (myData.getMonth() + 1) + '-' + myData.getDate(),//获取今天日期
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      order_id: options.order_id || '',
    })
    this.getContractInfo();//获取合同信息
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },


  // 添加图片 打开中间页
  openChoose(e) {
    var that = this;
    that.setData({
      isShow: true,
    })
  },
  //关闭中间页(关闭)
  closeShoot() {
    var that = this;
    that.setData({
      isShow: false,
    })
  },
  //打开相机
  openAbout(e) {
    var that = this;
    var type = e.currentTarget.dataset.type;
    //关闭中间页面 打开自定义照相机
    that.setData({
      isShow: false,
    })
    var length = that.data.fileUrl.length || 0;
    var count = 9 - length;
    var sourceType = '';
    if (type == 1) {
      sourceType = 'album';
    } else if (type == 2) {
      sourceType = 'camera';
    }
    wx.chooseImage({
      count: count,
      sizeType: ['compressed'],
      sourceType: [sourceType],
      success: function (res) {
        const tempFilePaths = res.tempFilePaths;
        wx.showLoading({
          mask: true,
          title: '上传中',
        })
        that.uploadFiles(tempFilePaths);
      },
    })
  },
  // 上传环境图 -多图
  uploadFiles(tempFilePaths) {
    console.log('获取到的数据你好', tempFilePaths)
    var that = this;
    for (var i in tempFilePaths) {
      wx.uploadFile({
        url: app.globalData._url + 'Common/Common/upload',
        filePath: tempFilePaths[i],
        name: 'file',
        success: function (result) {
          if (result.statusCode !== 200) {
            wx.hideLoading();
            app.showToast('上传失败,请重新上传');
            return
          }
          var data = JSON.parse(result.data);
          if (data.code == 1000) {
            var fileUrl = that.data.fileUrl;
            var file_url = that.data.file_url;
            var objall = { new_name: data.data.all_url, old_name: '', type: 'image' };
            var obj = { new_name: data.data.url, old_name: '', type: 'image' };
            fileUrl.push(objall);
            file_url.push(obj);
            that.setData({
              fileUrl,
              file_url
            })
            wx.hideLoading();
          } else {
            wx.hideLoading();
            app.showToast('上传失败,请重新上传');
          }
        },
        fail: function (fail) {
          wx.hideLoading();
          app.showToast('上传失败,请重新上传');
        }
      })
    }
  },
  
  // 图片放大预览
  previewImage(e) {
    wx.previewImage({
      current: e.currentTarget.dataset.url,
      urls: [e.currentTarget.dataset.url] // 需要预览的图片http链接列表
    })
  },
  // 删除图片
  reduceFile(e) {
    var fileUrl = this.data.fileUrl;
    var file_url = this.data.file_url;
    var index = e.currentTarget.dataset.index;
    fileUrl.splice(index, 1);
    file_url.splice(index, 1);
    this.setData({
      fileUrl,
      file_url
    })
  },

  //获取合同文件信息
  getContractInfo() {
    var that = this;
    app.ajax({
      url: 'User/Contract/getContractPdf',
      method: "POST",
      data: {
        work_order_id: that.data.order_id,
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            contractInfo: res.data.data,
          })
          that.getContractTypeList();//获取合同类型
        }
      }
    })
  },
  //获取合同类型
  getContractTypeList() {
    var that = this;
    app.ajax({
      url: 'Common/Common/getSearchConfig',
      method: "POST",
      data: { config_name:'contract_type'},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            typeList: res.data.data
          })
          var list = res.data.data;
          list.forEach((item,index) => {
            if (item.id == that.data.contractInfo.type) {
              that.setData({
                type_index: index
              })
            }
          })
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },
  //选择合同类型
  bindTypePicker(e){
    this.setData({
      type_index: e.detail.value,
    })
  },
  //签约信息输入
  bindRelatedInput(e) {
    var type = e.currentTarget.dataset.type;
    var info = 'contractInfo.' + type;
    this.setData({
      [info]: e.detail.value
    })
  },

  // 提交数据,上传合同
  submit(e) {
    var that = this;
    var val = e.detail.value;
    if (!val.title){
      app.showToast('请输入合同名称');
      return
    }
    if (val.type_index==='') {
      app.showToast('请选择合同类型');
      return
    }else{
      if (that.data.typeList[val.type].id > 2 && val.quota_money==''){
        app.showToast('请输入固定金额');
        return
      }
      if (that.data.typeList[val.type].id <= 2 && val.gas_fee=='') {
        app.showToast('请输入每月燃气均费');
        return
      }
    }
    if (!val.contract_party) {
      app.showToast('请输入签约方');
      return
    }
    if (!val.related_name) {
      app.showToast('请输入签约代表姓名');
      return
    }
    if (!val.related_tel) {
      app.showToast('请输入签约代表电话');
      return
    }
    if (!val.related_id_card) {
      app.showToast('请输入签约代表身份证号码');
      return
    }
    if (!val.effect_time) {
      app.showToast('请选择签约时间');
      return
    }
    if (that.data.file_url.length<=0){
      app.showToast('请上传合同附件');
      return
    }
    app.ajax({
      url: 'User/Spread/uploadContract',
      method: "POST",
      msg: '加载中...',
      data: {
        work_order_id: that.data.order_id,
        title: val.title,
        remark: val.remark,
        contract_enclosure: that.data.file_url,
        type: that.data.typeList[val.type].id,
        quota_money: that.data.typeList[val.type].id > 2 ? val.quota_money : '',
        contract_party: val.contract_party,
        related_name: val.related_name,
        related_tel: val.related_tel,
        related_id_card: val.related_id_card,
        effect_time: val.effect_time,
        gas_fee: that.data.typeList[val.type].id <= 2 ? val.gas_fee : '',
      },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast('上传成功');
          setTimeout(() => {
            wx.navigateBack();
          }, 1500)
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },
})