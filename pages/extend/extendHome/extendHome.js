// pages/user/extendHome/extendHome.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: '',
    contract: '',//合同详情
    accountInfo: '',//积分信息

    login_status: false,
    authInfo: { 'status_name': '待认证', status: '' },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    app.loginStatus(function () {
      that.setData({
        login_status: true
      })
      that.getUserInfo();
    }, function () {
      that.setData({
        login_status: false
      })
    })
    wx.stopPullDownRefresh();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /*** 页面相关事件处理函数--监听用户下拉动作*/
  onPullDownRefresh: function () {
    var that = this;
    console.log('下拉*******');
    that.onShow();
  },

  getUserInfo() {
    var that = this;
    app.ajax({
      url: 'User/Spread/getInfo',
      method: "POST",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            userInfo: res.data.data
          })
          that.dealerIncome();//获取兑换积分
          // 如果未签合同则弹起弹窗提示签合同
          if (res.data.data.is_sign_contract==0){
            wx.showModal({
              title: '',
              content: '你还未签合同,请先签合同',
              confirmText: '确定',
              confirmColor: '#DF0101',
              showCancel: false,
              success(res) {
                if (res.confirm) {
                  wx.navigateTo({
                    url: '../contract/contract',
                  })
                }
              },
            })
          }
        }
      }
    })
  },
  //获取用户认证信息
  getAuthInfo() {
    var that = this;
    app.ajax({
      url: "User/Realname/getAuthInfo",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            authInfo: res.data.data
          })
        } else {
          that.setData({
            authInfo: { 'status_name': '待认证', 'status': '' }
          })
        }
      }
    })
  },
  //跳转合同
  navContract() {
    var that = this;
    wx.navigateTo({
      url: '../contract/contract',
    })

  },
  // 获取推广列表
  dealerIncome() {
    var that = this;
    app.ajax({
      url: "User/Withdraw/getAccountInfo",
      data: { role: that.data.role },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            accountInfo: res.data.data,
          })
        }
      }
    })
  },

  // 兑换
  cashWithdrawal() {
    wx.navigateTo({
      url: '/pages/user/accounputForward/accounputForward?role=6',
    })
  },

  // 邀请好友
  promotionFriends() {
    var that = this;
    if (!that.data.login_status) {
      app.showToast('您还未登陆,请先登陆', 'none', 2000, that.toLogin());
      return false;
    }
    wx.navigateTo({
      url: '../inviteHome/inviteHome',
    })
  },

  // 我要送券
  giveCoupon() {
    wx.navigateTo({
      url: '../giveCoupon/giveCoupon',
    })
  },

  // 我的经销商码
  mycode() {
    wx.navigateTo({
      url: '../myQRcode/myQRcode',
    })
  },

  // 我的业绩
  seeAll() {
    wx.navigateTo({
      url: '../spreadRecord/spreadRecord',
    })
  },

  //跳转我要报单
  navOrderConfirm(){
    var that = this;
    if(!that.data.login_status){
      app.showToast('您还未登陆,请先登陆','none',2000,that.toLogin());
      return false;
    }
    wx.navigateTo({
      url: '../orderConfirm/orderConfirm',
    })
  },

  //跳转报单管理
  navOrderList(){
    var that = this;
    if(!that.data.login_status){
      app.showToast('您还未登陆,请先登陆','none',2000,that.toLogin());
      return false;
    }
    wx.navigateTo({
      url: '../myOrderList/myOrderList?client=5', //1用户端5经销商端
    })
  },
  myTameList(){
    var that = this;
    if(!that.data.login_status){
      app.showToast('您还未登陆,请先登陆','none',2000,that.toLogin());
      return false;
    }
    wx.navigateTo({
      url: '../../myTame/myTame', //1用户端5经销商端
    })
  },

  //跳转我的认证
  authentication() {
    var that = this;
    if (that.data.authInfo.status == -2) {
      wx.navigateTo({
        url: '../../certification/uploadDocuments/uploadDocuments',
      })
    } else if (that.data.authInfo.status == -3) {
      wx.navigateTo({
        url: '../../certification/faceRecognition/faceRecognition',
      })
    } else if (that.data.authInfo.status == 2 || that.data.authInfo.status == 1) {
      wx.navigateTo({
        url: '../../certification/myCertification/myCertification',
      })
    } else {
      wx.navigateTo({
        url: '../../certification/realName/realName',
      })
    }
  },
  //跳转设置
  navSetting(){
    wx.navigateTo({
      url: '../../user/changeMsg/changeMsg',
    })
  },
  //获取用户头像信息
  bindgetuserinfofun: function (e) {
    var that = this;
    if (e.detail.errMsg == 'getUserInfo:fail:cancel to confirm login' ||
      e.detail.errMsg == 'getUserInfo:fail auth deny' || e.detail.rawData == 'undefined') {
    } else {
      wx.setStorageSync('user_Info', e.detail.userInfo); //缓存user信息
      that.editUserInfo(e.detail.userInfo);
    }
  },
  //存储用户信息
  editUserInfo(userMsg) {
    var that = this;
    console.log('userMsg>>>', userMsg)
    app.ajax({
      url: "User/Spread/editInfo",
      data: {
        avatar_img: userMsg.avatarUrl,
        gender: userMsg.gender,
      },
      success: function (res) {
        console.log('获取用户信息的结果', res)
        if (res.data.code == 1000) {
          // 更新用户地址
          // that.updateAddress();
        } else {

        }
        that.getUserInfo(); //重新获取用户信息
      }
    })
  },
  toLogin(){
    wx.navigateTo({
      url: '/pages/user/login/login',
    })
  }

})
