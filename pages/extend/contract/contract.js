// pages/extend/contract/contract.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.getInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  //获取信息
  getInfo() {
    var that = this;
    app.ajax({
      url: "User/Spread/getContractInfo",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            info: res.data.data
          })
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },

  //签名
  sign() {
    var that = this;
    var name = that.data.info.second_party_name;
    var user_id = that.data.info.user_id;
    wx.navigateTo({
      url: '../sign/sign?user_id='+ user_id +'&name=' + name,
    })
  },

  // 查看大图
  previewImage(e) {
    var currentImg = e.currentTarget.dataset.url;
    wx.previewImage({
      current: currentImg, // 当前显示图片的http链接
      urls: [currentImg] // 需要预览的图片http链接列表
    })
  },
})