// pages/shopping/faceRecognition/faceRecognition.js
var app = getApp();
var overfun;
var authInfo= {};//认证信息
Page({
  /**
   * 页面的初始数据
   */
  data: {
    ctx: null,
    videoSrc: '', //视频路径
    times: '10',
    state: 0, //'0.还没有拍摄'  '1.拍摄中' 2.拍摄完毕
    verificationCode: 1234,
    codeArr: [],
    isModalShow: false,
    isHiden: false,
    name: '',
    id_card: '',
    subData: {},
    flag: true,
    stoping: true, //停止
    readNum: false, //是否读数字
    session_id: '', //获取唇语验证码时获得
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.ctx = wx.createCameraContext();
    // this.setData({
    //   name: options.name,
    //   id_card: options.id_number
    // });
    this.setData({
      subData: wx.getStorageSync('subAuthData'),
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    that.getAuthInfo();


  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    wx.stopBackgroundAudio();
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },


  /**
   * 用户点击开始拍摄
   */
  transcribe: function() {
    var that = this;
    var backgroundAudioManager = wx.getBackgroundAudioManager()
    // 设置了 src 之后会自动播放
    backgroundAudioManager.title = '音频';
    backgroundAudioManager.epname = '音频';
    backgroundAudioManager.singer = '音频';
    backgroundAudioManager.coverImgUrl = '';
    backgroundAudioManager.src = 'https://qn.youheone.com/authVideo.mp3';
    backgroundAudioManager.play();   
    if (that.data.times != 10) {
      app.showToast('停止');
      return false;
    }
    if (that.data.readNum) {
      that.setData({
        isModalShow: true,
        isHiden: true,
      })
      that.getCode();
    } else {
      that.sureTranscribe();
    }
  },

  // 关闭弹窗
  cancel: function() {
    var that = this;
    that.setData({
      isModalShow: false,
      isHiden: false
    })
  },

  // 用户点击 *记住了，开始录制*
  sureTranscribe: function() {
    var that = this;
    that.setData({
      videoSrc: '',
      isHiden: false,
      isModalShow: false,
    })
    that.ctx.startRecord({
      timeoutCallback: function(res) {
        that.stopVideo(); //停止拍摄
      },
      success: function(res) {
        that.setData({
          state: 1
        })
        that.addTime();
      },

    })
  },

  //增加时间
  addTime() {
    var that = this,
    count = 10;
    clearInterval(overfun);
    overfun = setInterval(function() {
      count--;
      console.log('计时', count);
      that.setData({
        times: count >= 10 ? count : '0' + count
      })
      if (count <= 0) {
        clearInterval(overfun);
        that.stopVideo(); //停止拍摄
      }
    }, 1000)
  },

  //结束停止 (用户不停止将自动停止)
  stopVideo() {
    var that = this;
    clearInterval(overfun);
    wx.stopBackgroundAudio();
    // wx.showLoading({
    //   title: '结束中',
    // })
    if (!that.data.stoping) {
      return false
    }
    that.setData({
      stoping: false,
    })
    that.ctx.stopRecord({
      success: function(res) {        
        that.setData({
          videoSrc: res.tempVideoPath,
          state: 2
        });
        // app.showToast('录制完成');
        // wx.hideLoading();       
        //clearInterval(overfun);//时间停止  
        wx.redirectTo({
          url: '../inAudit/inAudit?code=' + that.data.verificationCode + '&id_card=' + that.data.id_card + '&name=' + that.data.name + '&session_id=' + that.data.session_id + '&videoSrc=' + that.data.videoSrc
        })
      },
      fail: function(res) {
        console.log('停止-----调用借口失败', res);
        that.stopVideo();//重调停止事件
      },
      complete: function(res) {
        console.log('既不成功  也不失败',res)
      }
    })
  },
  //用户不允许使用摄像头
  error(e) {
    app.showToast('不允许使用摄像头将无法使用', "none", 4000, function() {});
  },

  getCode() {
    var that = this;
    app.ajax({
      url: 'Common/Face/livegetfour',
      data: {},
      success: function(res) {
        if (res.data.code == 1000) {
          var verificationCode = res.data.data.code;
          var session_id = res.data.data.session_id;
          var arr = verificationCode.split('');
          var codeArr = [];
          for (var i in arr) {
            codeArr.push(arr[i]);
          }
          that.setData({
            verificationCode: verificationCode,
            codeArr: codeArr,
            session_id: session_id
          })

        } else {
          app.showToast(res.data.message, "none", 3000, function() {});
        }
      }
    })
  },
  //非正常终止摄像头
  otherOver(e) {
    console.log('非正常', e)
  },
  //重新拍摄
  again() {
    var that = this;
    if (!that.data.flag) {
      return false;
    }
    var backgroundAudioManager = wx.getBackgroundAudioManager()
    // 设置了 src 之后会自动播放
    backgroundAudioManager.title = '音频';
    backgroundAudioManager.epname = '音频';
    backgroundAudioManager.singer = '音频';
    backgroundAudioManager.coverImgUrl = '';
    backgroundAudioManager.src = 'https://qn.youheone.com/authVideo.mp3';
    backgroundAudioManager.play();
    that.setData({
      videoSrc: '',
      state: 1,
      ctx: null,
      times: '00',
      stoping: true
    })
    that.sureTranscribe(); //重新拍摄
  },

  //获取用户认证信息
  getAuthInfo() {
    var that = this;
    app.ajax({
      url: "User/Realname/getAuthInfo",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          authInfo= res.data.data;
          that.setData({
            name: res.data.data.realname,
          })
        }
      }
    })
  }
})