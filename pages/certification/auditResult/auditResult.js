// pages/shopping/inAudit/inAudit.js
var app = getApp();
var shsuccess = app.globalData._network_path + 'shsuccess.png';
Page({
  /**
   * 页面的初始数据
   */
  data: {
    shsuccess: shsuccess,
    rotate: 0,
    result: 1,
    message: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.setData({
      result: options.result,
    });
    setInterval(function() {
      if (that.data.rotate == 0) {
        that.setData({
          rotate: 90
        })
      } else if (that.data.rotate == 90) {
        that.setData({
          rotate: 0
        })
      }
    }, 1000)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  // 重新认证
  again() {
    wx.redirectTo({
      url: '../faceRecognition/faceRecognition',
    })
  },

  // 暂不认证
  nope() {
    wx.switchTab({
      url: '../../user/userHome/userHome',
    })
  },
  //查看认证信息
  seeInfo() {
    var that = this;
    wx.redirectTo({
      url: '../../myCertification/myCertification',
    })
  },
  //回到首页
  gohome() {
    wx.switchTab({
      url: '/pages/home/home',
    })
  }
})