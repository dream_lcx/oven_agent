// pages/myTame/myTame.js
var WxParse = require('../../wxParse/wxParse.js');
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 30;
var bg = app.globalData._network_path + 'invite_bg.png';
var userInfo= [];
Page({

  /**
   * 页面的初始数据
   */
  data: {
    invitationList: [],
    hasMore: false,
    load: true,
    loading: false,
    load_show:false,
    bg: bg,
    applyInfo: '', //申请信息
    isApply: false,//是否申请过
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.getmyinvitation(); //获取邀请列表
    that.getUserInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    _PAGE = 1;
    that.setData({
      invitationList:[]
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    if (!that.data.hasMore) {
      return false;
    }
    _PAGE++;
    that.setData({
      loading: true
    })
    that.getmyinvitation();
  },

  // 获取邀请好友列表
  getmyinvitation() {
    var that = this;
    app.ajax({
      url: 'User/Useraddress/getInvitationRecord',
      method: "POST",
      data: {
        page: _PAGE,
        pageSize: _PAGESIZE,
      },
      success: function(res) {
        if (res.data.code == 1000) {
          let gitdata=res.data.data.data;
          gitdata.forEach(element => {
            element.curTab=false
          });
          if (_PAGE == 1) {
            that.setData({
              invitationList: gitdata
            })
          } else {
            that.setData({
              invitationList: that.data.invitationList.concat(gitdata)
            })
          }
          console.log('中间的数据',that.data.invitationList)
          // 是否加载更多
          var hasMore = true;
          if (gitdata.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            load: false,
            loading: false,
            load_show:true
          });
        } else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              load_show:true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false
            })
          }
        }
      }
    })
  },

  // 获取申请信息
  getApply() {
    var that = this;
    app.ajax({
      url: 'User/Spread/getApply',
      method: "POST",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            applyInfo: res.data.data,
            isApply: true,
          })
        }else{
          that.setData({
            isApply: false,
          })
        }
      }
    })
  },
  // 申请经销商
  applyDistributor() {
    var that = this;
    var is_auth = userInfo.is_auth;
    if (is_auth == 1) {
      wx.showModal({
        content: '请先进行实名认证',
        showCancel: false,
        success(res) {
          if (res.confirm) {
            //跳转认证
            that.authentication();
          }
        }
      })
    } else if (is_auth == 2) {
      if (!that.data.isApply){
        wx.navigateTo({
          url: '../distributorApply/distributorApply',
        })
      }else{
        wx.navigateTo({
          url: '../distributorApplyResult/distributorApplyResult',
        })
      }
      
    }

  },

  // 邀请好友
  invitation: function() {   
    var that = this;
    wx.navigateTo({
      url: '../../marketing/marketing',
    })
    // var is_rent_eq = userInfo.is_rent_eq;
    // if (!is_rent_eq){
    //   wx.showModal({
    //     content: '签约后才可进行分享',
    //     showCancel: false,
    //     success(res) {
    //       if (res.confirm) {
            
    //       }
    //     }
    //   })
    // }else{
    //   wx.navigateTo({
    //     url: '../../marketing/marketing',
    //   })
    // }
  },

  getUserInfo() {
    var that = this;
    app.ajax({
      url: 'User/User/getUserInfo',
      method: "POST",
      data: {},
      success: function(res) {
        if (res.data.code == 1000) {
          userInfo= res.data.data
        }
      }
    })
  },
  //打开更多
  openMore(e){
    var i = e.currentTarget.dataset.index;
    var invitationList=this.data.invitationList;
    console.log(i,invitationList)
    if(invitationList[i].interpulsion.length<=0){
      return false
    }
    invitationList[i].curTab=!invitationList[i].curTab
    this.setData({
      invitationList
    })
  }
})