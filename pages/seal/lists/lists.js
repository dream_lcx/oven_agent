// pages/seal/lists/lists.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
      lists:[],
      loading:true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getLists();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
 getLists(){
   var that = this;
   app.ajax({
     url: 'User/Seal/getLists',
     data: { },
     success: function (res) {
       if (res.data.code == 1000) {
          that.setData({
            lists:res.data.data,
            loading:false
          })
        
       }
     }
   })
 },
 //创建印章
 add(){
   wx.navigateTo({
     url: '../add/add',
   })
 },
  //大图预览
  previewImg(e){
    wx.previewImage({
      urls: [e.currentTarget.dataset.url] // 需要预览的图片http链接列表
    })
  },
 //删除公章
  delSeal(e){
    var that = this;
    var seal_id = e.currentTarget.dataset.seal_id;
    var index = e.currentTarget.dataset.index;
    wx.showModal({
      title: '提示',
      content: '是否确认删除',
      confirmColor: '#c80000',
      success: function (res) {
        if (res.confirm) {
          app.ajax({
            url: 'User/Seal/del',
            data: {
              seal_id: seal_id
            },
            success: function (res) {
              app.showToast(res.data.message);
              if (res.data.code == 1000) {
                var lists = that.data.lists;
                lists.splice(index, 1);
                that.setData({
                  lists:lists
                })
              }
            }
          })

        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  }
})