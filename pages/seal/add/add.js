// pages/seal/add/add.js
var red_color = 'red';
var blue_color = 'blue';
var black_color = 'black';
var app = getApp();
var util = require("../../../utils/util.js");
var flag = false;
var seal_oval = app.globalData._network_path + 'seal_oval.png';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    seal_oval: seal_oval,
    choose_color:red_color,//选中的印章颜色
    choose_seal: 'star',//选中的印章样式 star圆形 oval椭圆形
    red_color: red_color,//红色
    blue_color: blue_color,//蓝色
    black_color: black_color,//黑色
    h_text: '',//横向文
    q_text:'',//下弦文
    preview_image:"../../../images/seal_circle.png",//预览图
    preview_image_url:'',//印章路径
    account_id:'',//e签宝唯一标识
  },
  //横向文输入
  inputHText(e) {
    var that = this;
    this.setData({
      h_text: e.detail.value
    })
    that.createSeal();
  },
  //下弦文输入
  inputQText(e) {
    var that = this;
    this.setData({
      q_text: e.detail.value
    })
    that.createSeal();
  },
//选择颜色
  chooseColor(e){
    var choose_color = e.currentTarget.dataset.color;
    var that = this;
    that.setData({
      choose_color: choose_color
    })
    that.createSeal();
  },
  //选择样式
  chooseSeal(e){
    var choose_seal = e.currentTarget.dataset.seal_style;
    var that = this;
    that.setData({
      choose_seal: choose_seal
    })
    that.createSeal();
  },
  //生成印章
  createSeal(){
    var that = this;
    var template_type = that.data.choose_seal;
    var color = that.data.choose_color;
    var h_text = that.data.h_text;
    var q_text = that.data.q_text;
    app.ajax({
      url: 'User/Seal/appointCompanySeal',
      data: { 'template_type': template_type, 'color': color, 'h_text': h_text, 'q_text': q_text},
      success: function (res) {
        if(res.data.code==1000){
          that.setData({
            preview_image: res.data.data.all_url,
            preview_image_url:res.data.data.url,
            account_id: res.data.data.accountId

          })
        }else{
          app.showToast(res.data.message);
        }
      }
    })
  },
  //提交数据
  add(e){
    var that = this;
    var template_type = that.data.choose_seal;
    var color = that.data.choose_color;
    var h_text = that.data.h_text;
    var q_text = that.data.q_text;
    var name = e.detail.value.name;
    if (!util.isExitsVariable(name)){
      app.showToast("请输入印章名称");
      return false;
    }
    if (flag) {
      return false;
    }
    flag = true;
    app.ajax({
      url: 'User/Seal/add',
      data: { 'template_type': template_type, 'color': color, 'h_text': h_text, 'q_text': q_text, name: name, seal: that.data.preview_image_url, account_id:that.data.account_id },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.message,'none',3000,function(){
            wx.redirectTo({
              url: '../lists/lists',
            })
          });
        }else{
          app.showToast(res.data.message);
        }
        flag = false;
      }
    })
  }

})