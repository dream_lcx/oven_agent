// pages/setPosition/setPosition.js
// pages/login/login.js
var app = getApp();
var loginimg = app.globalData._url + 'static/weapp/loginbg.jpg';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    login:false,
    valueName:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    that.headid = this.selectComponent("#addre_id"); //引入地址    
    this.headid.startAddressAnimation(true);//打開地址
  },
  onUnload: function () {
    wx.setStorageSync('login_back', true);
  },
  colseFun:function(){
    this.headid.startAddressAnimation(true);//打開地址
    return false;
  },
  // 点击地区选择确定按钮
  getSure: function (data) {
    var that = this;
    this.headid.startAddressAnimation(true);//打開地址
    that.setData({
      valueName: data.detail.areaName,
      areaInfo_name: data.detail.areaName
    })
    var areaInfo_name = data.detail.areaName;
    var area_id_Info = data.detail.areaId;
    var content = "是否确认当前所在地理位置为" + areaInfo_name[0] + "-" + areaInfo_name[1] + "-" + areaInfo_name[2] + ",一旦设置成功不能再次进行修改";
    wx.showModal({
      title: '',
      content: content,
      showCancel: true,
      success: function (res) {
        if (res.confirm) {
            wx.setStorageSync('is_change', true);
          wx.setStorage({
            key: 'area_id',
            data: area_id_Info,
          });
          wx.setStorage({
            key: 'areaInfo_name',
            data: areaInfo_name
          });
          that.setData({
            areaInfo_name: areaInfo_name,
            area_id_Info: area_id_Info,
            login: true
          })
          // that.setData({
          //   condition: !that.data.condition,
          //   login: true
          // })
          wx.navigateBack({
            delta: 1,
          })
        } else if (res.cancel) {
          that.setData({
            login: true
          })
        }
      }
    })
    
  },


})