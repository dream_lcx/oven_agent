// pages/inviteFriends/inviteFriends.js
var WxParse = require('../../../wxParse/wxParse.js');
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 30;
var bg = app.globalData._network_path + 'invite_bg.png';
var userInfo= [];
Page({

  /**
   * 页面的初始数据
   */
  data: {
    invitationList: [],
    hasMore: false,
    load: true,
    loading: false,
    bg: bg,
    authInfo: { 'status_name': '待认证', status: '' },
    applyInfo: '', //申请信息
    isApply: false,//是否申请过
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.getApply(); //获取申请信息
    that.getmyinvitation(); //获取邀请列表
    that.getArticle();
    that.getUserInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    _PAGE = 1;
    that.setData({
      invitationList:[]
    })
    that.getAuthInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    if (!that.data.hasMore) {
      return false;
    }
    _PAGE++;
    that.setData({
      loading: true
    })
    that.getmyinvitation();
  },

  // 获取邀请好友列表
  getmyinvitation() {
    var that = this;
    app.ajax({
      url: 'User/Useraddress/getInvitationRecord',
      method: "POST",
      data: {
        page: _PAGE,
        pageSize: _PAGESIZE,
      },
      success: function(res) {
        if (res.data.code == 1000) {
          if (_PAGE == 1) {
            that.setData({
              invitationList: res.data.data.data
            })
          } else {
            that.setData({
              invitationList: that.data.invitationList.concat(res.data.data.data)
            })
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            load: false,
            loading: false
          });
        } else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false
            })
          }
        }
      }
    })
  },

  // 获取申请信息
  getApply() {
    var that = this;
    app.ajax({
      url: 'User/Spread/getApply',
      method: "POST",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            applyInfo: res.data.data,
            isApply: true,
          })
        }else{
          that.setData({
            isApply: false,
          })
        }
      }
    })
  },
  // 申请经销商
  applyDistributor() {
    var that = this;
    var is_auth = userInfo.is_auth;
    if (is_auth == 1) {
      wx.showModal({
        content: '请先进行实名认证',
        showCancel: false,
        success(res) {
          if (res.confirm) {
            //跳转认证
            that.authentication();
          }
        }
      })
    } else if (is_auth == 2) {
      if (!that.data.isApply){
        wx.navigateTo({
          url: '../distributorApply/distributorApply',
        })
      }else{
        wx.navigateTo({
          url: '../distributorApplyResult/distributorApplyResult',
        })
      }
      
    }

  },

  // 邀请好友
  invitation: function() {   
    var that = this;
    wx.navigateTo({
      url: '../../marketing/marketing',
    })
    // var is_rent_eq = userInfo.is_rent_eq;
    // if (!is_rent_eq){
    //   wx.showModal({
    //     content: '签约后才可进行分享',
    //     showCancel: false,
    //     success(res) {
    //       if (res.confirm) {
            
    //       }
    //     }
    //   })
    // }else{
    //   wx.navigateTo({
    //     url: '../../marketing/marketing',
    //   })
    // }
  },
  // 获取邀请规则
  getArticle: function() {
    var that = this;
    // Ajax请求
    app.ajax({
      url: 'Common/Article/getArticle',
      data: {
        cate_sn: "ApPcTPn3"
      },
      success: function(res) {
        if (res.data.code == 1000) {
          var content = res.data.data[0].content;
          WxParse.wxParse('article', 'html', content, that, 5);
        }
        console.log('请求结果', that.data.content);
      }
    })
  },

  getUserInfo() {
    var that = this;
    app.ajax({
      url: 'User/Spread/getInfo',
      method: "POST",
      data: {},
      success: function(res) {
        if (res.data.code == 1000) {
          userInfo= res.data.data
        }
      }
    })
  },
  //跳转我的认证
  authentication() {
    var that = this;
    if (that.data.authInfo.status == -2) {
      wx.navigateTo({ //../../shopping/uploadDocuments/uploadDocuments
        url: '../../shopping/uploadDocuments/uploadDocuments',
      })
    } else if (that.data.authInfo.status == -3) {
      wx.navigateTo({
        url: '../../shopping/faceRecognition/faceRecognition',
      })
    } else if (that.data.authInfo.status == 2 || that.data.authInfo.status == 1) {
      wx.navigateTo({
        url: '../../myCertification/myCertification',
      })
    } else {
      wx.navigateTo({
        url: '../../shopping/realName/realName',
      })
    }

  },
  //获取用户认证信息
  getAuthInfo() {
    var that = this;
    app.ajax({
      url: "User/Realname/getAuthInfo",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            authInfo: res.data.data
          })
        }
      }
    })
  }
})