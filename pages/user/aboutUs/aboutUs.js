// pages/user/aboutUs/aboutUs.js
var app = getApp();
var WxParse = require('../../../wxParse/wxParse.js');
Page({
  

  /**
   * 页面的初始数据
   */
  data: {
    cate_sn: 'A6WKb4ja', // 位码
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.getArticle();  // 获取关于我们
  },

  // 获取关于我们
  getArticle: function () {
    var that = this;
    // Ajax请求
    app.ajax({
      url: 'Common/Article/getArticle',
      data: { cate_sn: that.data.cate_sn },
      success: function (res) {
        if (res.data.code == 1000) {
          var content = res.data.data[0].content;
          WxParse.wxParse('article', 'html', content, that, 5);
        }
      }
    })
  }


})