// pages/shopping/payment/payment.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    payment_icon: '../../../images/payment_icon.png',
    choose_r: '../../../images/choose_r.png',
    order_id:'',  // 订单id
    equipment_id: '',//缴费器id
    contract_id:'',
    orderInfo:{}, // 订单详情
    pay_way:1,    // 支付方式
    type:'',
    pay_list:[
      {
        id:0,
        pay_way:1,
        name:'线上支付',
        tip:'使用微信支付',
        pic:'../../../images/weixin.png',
        checked:true
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var that = this;
    that.setData({
      order_id: options.order_id,
      type: options.type,
      equipment_id: options.equipment_id ? options.equipment_id:'',
      contract_id: options.contract_id ? options.contract_id : '',
    })
    that.getOrderInfo(); // 获取订单详情
  },

  // 查询订单详情
  getOrderInfo: function () {
    var that = this;
    var order_id = that.data.order_id;  // 订单ID
    app.ajax({
      url: 'User/Renew/getRenewOrderDetail',
      data: { renew_order_id: order_id },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            orderInfo: res.data.data
          });
        } else {
          app.showToast(res.data.message);
        }
        console.log('订单详情',res);
      }
    })
  },

  //支付
  payment: function () {
    // wx.navigateTo({
    //   url: '../paySuccess/paySuccess?type="ok"',
    // })

    var that = this;
    var order_id = that.data.order_id;  // 订单ID
    var pay_way = that.data.pay_way;  // 支付方式
    // Ajax请求
    app.ajax({
      url: 'User/Renew/renewOrderPay',
      data: { renew_order_id: order_id, pay_way: pay_way },
      success: function (res) {
        console.log(res);
        if (res.data.code == 1000) {

          var jsapi = res.data.data.jsapi;
          var equipment_id =res.data.data.equipment_id;
          var company_id = res.data.data.company_id;
          wx.requestPayment({
            timeStamp: jsapi.timeStamp,
            nonceStr: jsapi.nonceStr,
            package: jsapi.package,
            signType: jsapi.signType,
            paySign: jsapi.paySign,
            'success': function (res) {
              app.showToast("支付成功");
              if (that.data.type==1){

                wx.redirectTo({
                  url: '../../user/renewPaySuccess/renewPaySuccess?order_id=' + order_id + '&equipment_id=' + that.data.equipment_id,
                })
              } else if (that.data.type == 2){
                wx.redirectTo({
                  url: '../../user/renewPaySuccess/renewPaySuccess?order_id=' + order_id + '&equipment_id=' + that.data.equipment_id,
                  // url: '../econtract/econtract?contract_id=' + that.data.contract_id,
                })
              }else{
                wx.switchTab({
                  url: '/user/userHome/userHome'
                })
              }

            },
            'fail': function (res) {
              app.showToast("支付失败");
              console.log('sbai',res);
            }
          })
        } else if (res.data.code == 1001){
          app.showToast("支付成功");
          wx.redirectTo({
            url: '../../user/renewPaySuccess/renewPaySuccess?order_id=' + order_id + '&equipment_id=' + that.data.equipment_id,
          })

        }else{
          app.showToast(res.data.message);
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  //选择支付方法
  radioChange:function(e){
    //console.log(e.detail.value);
  },

})
