// pages/user/myReport/myReport.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    phonenum: '',
    idcard: '',
    reason: '',
    isDisable: false,
    reportDes: '报备'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  // 报备
  formSubmit(e) {
    var that = this;
    var name = e.detail.value.name;
    var phonenum = e.detail.value.phonenum;
    var idcard = e.detail.value.idcard;
    var reason = e.detail.value.reason;
    that.setData({
      isDisable: true
    })
    app.ajax({
      url: 'User/Spread/applyReport',
      method: "POST",
      data: {
        username: name,
        phone: phonenum,
        id_card: idcard,
        note: reason
      },
      success: function(res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.message);
          that.setData({
            reportDes: '已报备'
          })
          setTimeout(function() {
            wx.navigateBack({
              delta: 1,
            })
          }, 1000)
        } else {
          app.showToast(res.data.message);
          that.setData({
            isDisable: false
          })
        }
      }
    })
  }
})