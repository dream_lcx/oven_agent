// pages/user/myInvite/myInvite.js
var app = getApp();
var page=1;
var allback2 = app.globalData._network_path + 'allback3.png';
var load=true;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    allback: allback2,
    hasMore: false,
    loading: false,
    pagesize: 30,
    inviteList:[],
    total_num:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    
    page=1
    this.setData({
      inviteList:[]
    })
    this.getInvite();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    if (!that.data.hasMore) {
      return false;
    }
    page+=1;
    that.setData({
      loading: true
    })
    that.getInvite()
  },


  // 获取邀请列表
  getInvite() {
    var that = this;
    app.ajax({
      url: 'User/Useraddress/getInvitationRecord',
      method: "POST",
      data: {
        page: page,
        pageSize: that.data.pagesize
      },
      success: function(res) {
        if (res.data.code == 1000) {
          if (that.data.page == 1) {
            console.log('结果', res.data.data.data);
            that.setData({
              inviteList: res.data.data.data,
              total_num: res.data.data.total_num
            })
          } else {
            that.setData({
              inviteList: that.data.inviteList.concat(res.data.data.data),
              total_num: res.data.data.total_num
            })
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.data.length < that.data.pagesize) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          load=false;
        } else {
          if (that.data.page == 1) {
            that.setData({
              hasMore: false,
              loading: false
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false
            })
          }
        }
      }
    })
  }
})