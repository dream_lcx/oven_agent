// pages/user/accounputForward/accounputForward.js
var app = getApp();
var WxParse = require('../../../wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    accountInfo: '',
    role: 6,// 角色,4:用户,6:经销商,默认6
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      role: options.role
    })
    // this.workerInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.workerInfo();
    this.getRule();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  // 兑换
  withdrawal(e) {
    var that = this;
    wx.navigateTo({
      url: '../putForwardPrice/putForwardPrice?role=' + that.data.role
    })
    // var is_have_bank = that.data.accountInfo.is_have_bank;
    // if (is_have_bank){
    //   wx.navigateTo({
    //     url: '../putForwardPrice/putForwardPrice?role=' + that.data.role
    //   })
    // }else{
    //   wx.showModal({
    //     content: '请先绑定银行卡',
    //     showCancel: false,
    //     success(res) {
    //       if (res.confirm) {
    //         wx.navigateTo({
    //           url: '../addeditAccount/addeditAccount?type=2&role=' + that.data.role
    //         })
    //       }
    //     }
    //   })
    // }

  },

  // 获取经销商信息
  workerInfo() {
    var that = this;
    app.ajax({
      url: "User/Withdraw/getAccountInfo",
      data: { role: that.data.role },
      success: function(res) {
        if (res.data.code == 1000) {
          that.setData({
            accountInfo: res.data.data,
          })
        }
      }
    })
  },

  // 兑换记录
  presentRecord(){
    var that = this;
    wx.navigateTo({
      url: '../presentRecord/presentRecord?role=' + that.data.role,
    })
  },

  // 兑换账户设置
  presentAccount(){
    var that = this;
    wx.navigateTo({
      url: '../presentAccount/presentAccount?role=' + that.data.role,
    })
  },

  // 获取兑换说明
  getRule() {
    var that = this;
    app.ajax({
      url: 'Common/Article/getArticle',
      data: {
        cate_sn: 'AVwsyFwB'
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var article = res.data.data[0].content;
          WxParse.wxParse('article', 'html', article, that, 5);
        }
      }
    })
  },
})
