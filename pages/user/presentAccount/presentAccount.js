// pages/user/presentAccount/presentAccount.js
var app = getApp();
var load= true;
var page= 1,row= 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bankList:[],
    hasMore: false,
    loading: false,
    is_load:false,

    role: 6,// 角色,4:用户,6:经销商,默认6
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      role: options.role,
    })
    this.getbankList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      bankList:[]
    })
    page= 1;
    this.getbankList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if (!that.data.hasMore) {
      return false;
    }
    page++;
    that.setData({
      loading: true
    })
    that.getbankList();
  },  


  // 添加银行卡
  addAccount: function () {
    wx.navigateTo({
      url: '../addeditAccount/addeditAccount?type=2&role=' + this.data.role,
    })
  },

  // 编辑
  eidtcard: function (e) {
    var bank_id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../addeditAccount/addeditAccount?type=1&&bank_id=' + bank_id,
    })
  },

  // 删除
  delcard: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var bank_id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '是否删除该账户',
      success: function (res) {
        if (res.confirm) {
          var bankList = that.data.bankList         
          app.ajax({
            url: "User/Withdraw/deleteBank",
            data: {
              bank_id: bank_id
            },
            success: function (res) {
              app.showToast(res.data.message)   
              bankList.splice(index, 1);
              that.setData({
                bankList: bankList
              });        
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  // 获取银行卡列表
  getbankList(){
    var that = this;
    app.ajax({
      url: 'User/Withdraw/bankList',
      method: "POST",
      data: {
        role: that.data.role,
        page: page,
        row: row,
      },
      success: function (res) {
        if (res.data.code == 1000) {
          if (page == 1) {
            that.setData({
              bankList: res.data.data.data
            })
          } else {
            that.setData({
              bankList: that.data.bankList.concat(res.data.data.data)
            })
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.data.length < row) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          load= false;
        } else {
          if (page == 1) {
            that.setData({
              hasMore: false,
              loading: false
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
            })
          }
        }
        that.setData({
          is_load:true
        })
      }
    })
  }

})