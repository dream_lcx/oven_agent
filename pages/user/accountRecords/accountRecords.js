// pages/myCenter/accountRecords/accountRecords.js
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    sourceArr:[
      {
        id: 2,
        name: '全部来源'
      },
      {
        id: 3,
        name: '结算'
      },
      {
        id: 5,
        name: '兑换'
      },
    ],//来源数组
    sourceIndex: '',//选择的来源索引
    dateStart: '',//开始时间
    dateEnd: '' ,//结束
    expend: '4569.05',//支出
    income: '5039.93',//收入
    listArr:[
      {
        type: 1,
        source: '结算',
        time: '2019-5-4 10:22:10',
        remarks: '这是一个备注',
        money: '30.00'
      },
      {
        type: 2,
        source: '结算',
        time: '2019-5-4 10:22:10',
        remarks: '这是一个备注',
        money: '6000.00'
      },
      {
        type: 2,
        source: '兑换',
        time: '2019-5-4 10:22:10',
        remarks: '这是一个备注',
        money: '100.00'
      },
      {
        type: 1,
        source: '结算',
        time: '2019-5-4 10:22:10',
        remarks: '这是一个备注',
        money: '320.00'
      },
      {
        type: 1,
        source: '结算',
        time: '2019-5-4 10:22:10',
        remarks: '这是一个备注',
        money: '30.00'
      },
      {
        type: 2,
        source: '结算',
        time: '2019-5-4 10:22:10',
        remarks: '这是一个备注',
        money: '60.00'
      },
      {
        type: 2,
        source: '兑换',
        time: '2019-5-4 10:22:10',
        remarks: '这是一个备注',
        money: '100.00'
      },
      {
        type: 1,
        source: '结算',
        time: '2019-5-4 10:22:10',
        remarks: '这是一个备注',
        money: '320.00'
      },
      {
        type: 2,
        source: '兑换',
        time: '2019-5-4 10:22:10',
        remarks: '这是一个备注',
        money: '100.00'
      },
      {
        type: 1,
        source: '结算',
        time: '2019-5-4 10:22:10',
        remarks: '这是一个备注',
        money: '320.00'
      },
    ],//记录列表--模拟数据
    list:[],//记录列表
    empty: true,//空
    hasMore: false,//是否还有更多
    loading: false,//加载中
    loadShow: false,//加载数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    _PAGE = 1;
    that.searchFun();

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    _PAGE = 1;
    that.setData({
      sourceIndex: '',
      dateStart: '',
      dateEnd: '',
    })
    that.searchFun();


  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    _PAGE++;
    that.searchFun();

  },
  // 选择来源
  selectSource(e){
    var that = this;
    var value = e.detail.value;
    that.setData({
      sourceIndex: value
    })
    _PAGE = 1;
    that.searchFun();
  },
  // 选择开始时间
  selectDateStart(e){
    var that = this;
    var value = e.detail.value;
    that.setData({
      dateStart: value
    })
    _PAGE = 1;
    that.searchFun();
  },
  // 选择开始时间
  selectDateEnd(e) {
    var that = this;
    var value = e.detail.value;
    that.setData({
      dateEnd: value
    })
    _PAGE = 1;
    that.searchFun();
  },
  // 跳转详情
  goDetail(e){
    wx.navigateTo({
      url: '../accountRecordsDetail/accountRecordsDetail',
    })
  },
  // 搜索
  searchFun(){
    var that = this;
    var sourceIndex = that.data.sourceIndex;
    var sourceId = sourceIndex?that.data.sourceArr[sourceIndex].id:'';
    var dateStart = that.data.dateStart;
    var dateEnd = that.data.dateEnd;
    console.log('搜索函数>>>>>>>>>>>');

    that.setData({
      loading: true
    })

    app.ajax({
      url: 'User/Useraddress/getInvitationRecord',
      data: {
        page: _PAGE,
        pageSize: _PAGESIZE,
      },
      success: function (res) {
        if (res.data.code == 1000) {
          if (_PAGE == 1) {
            that.setData({
              // list: res.data.data,
              list: that.data.listArr,
              empty: false
            });
          } else {
            that.setData({
              // list: that.data.list.concat(res.data.data),
              list: that.data.list.concat(that.data.listArr),
              empty: false
            });
          }
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
        } else {
          wx.hideLoading();
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        wx.stopPullDownRefresh();
        that.setData({
          loadShow: true
        })
      }
    })
  }
})
