// pages/user/invitaList/invitaList.js
var app = getApp();
var guessImg = app.globalData._network_path + 'backshow.png';
var authInfo= { 'status_name': '待认证', status: '' };
Page({
  /**
   * 页面的初始数据
   */
  data: {
    height: 500,
    invitationList: [],
    banner: guessImg,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var query = wx.createSelectorQuery(),
      allheight = wx.getSystemInfoSync().windowHeight,
      that = this;
    //选择id
    query.select('#top_box').boundingClientRect();
    query.exec(function(res) {
      //res就是 所有标签为mjltest的元素的信息 的数组
      //取高度
      var getheight = allheight - res[0].height;
      that.setData({
        height: getheight
      })
    });
    that.getmyinvitation(); //获取邀请列表
    that.getAuthInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
  
  integralInfo: function() {
    wx.navigateTo({
      url: '../integralInfo/integralInfo',
    })
  },

  invitation: function() {
    wx.navigateTo({
      url: '../../marketing/marketing',
    })
  },

  getmyinvitation() {
    var that = this;
    app.ajax({
      url: 'User/Useraddress/getInvitationRecord',
      method: "POST",
      data: {},
      success: function(res) {
        if (res.data.code == 1000) {
          that.setData({
            invitationList: res.data.data.data
          })
        } else {
          app.showToast(res.data.message, "none", 2000, function() {});
        }
      }
    })
  },
  
  //图片报错  
  avatar_error(e) {
    var that = this,
      invitationList = that.data.invitationList,
      inds = e.currentTarget.dataset.index;
    invitationList[inds].avatar_img = '../../../images/user_pic.png'
    that.setData({
      invitationList: invitationList
    })
  },

  // 申请经销商
  applyDistributor() {
    var that = this;
    app.ajax({
      url: 'User/User/getUserInfo',
      method: "POST",
      data: {},
      success: function(res) {
        if (res.data.code == 1000) {
          var is_auth = res.data.data.is_auth         
          if (is_auth == 1) {
            wx.showModal({
              content: '请先进行实名认证',
              showCancel: false,
              success(res) {
                if (res.confirm) {
                  that.goRealname();
                }
              }
            })
          } else if (is_auth == 2) {
            console.log(is_auth)
            wx.navigateTo({
              url: '../distributorApply/distributorApply',
            })
          }
        }
      }
    })
  },
  //跳转我的认证
  goRealname() {
    var that = this;
    if (authInfo.status == -2) {
      wx.navigateTo({ //../../shopping/uploadDocuments/uploadDocuments
        url: '../../shopping/uploadDocuments/uploadDocuments',
      })
    } else if (authInfo.status == -3) {
      wx.navigateTo({
        url: '../../shopping/faceRecognition/faceRecognition',
      })
    } else if (authInfo.status == 2 || authInfo.status == 1) {
      wx.navigateTo({
        url: '../../myCertification/myCertification',
      })
    } else {
      wx.navigateTo({
        url: '../../shopping/realName/realName',
      })
    }

  },
  //获取用户认证信息
  getAuthInfo() {
    var that = this;
    app.ajax({
      url: "User/Realname/getAuthInfo",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          authInfo = res.data.data
        } else {
          authInfo = { 'status_name': '待认证', 'status': '' }
        }
      }
    })
  }
  
})