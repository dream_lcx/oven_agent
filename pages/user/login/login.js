// pages/user/login/login.js
var mta = require('../../../utils/mta_analysis.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loginBg: app.globalData._network_path + 'login_bg1.png',
    userInfo: '', //获取到的用户信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    mta.Page.init();
  },
  onShow() {
    wx.setStorageSync('is_login', false);
    app.wxLogin();
  },

  //注册
  register() {
    wx.navigateTo({
      url: '../register/register',
    })
  },

  //获取用户手机号码
  getPhoneNumber: function(e) {
    var iv = e.detail.iv,
      encryptedData = e.detail.encryptedData,
      that = this;
    console.log(e)
    if (e.detail.errMsg == 'getPhoneNumber:fail:cancel to confirm login' ||
      e.detail.errMsg == 'getPhoneNumber:fail user deny' || e.detail.rawData == 'undefined') {
      app.showToast('拒绝授权,将无法使用一键登录功能', "none", 2000, function() {});
      return false;
    } else if (e.detail.errMsg == "getPhoneNumber:fail 用户未绑定手机，请先在微信客户端进行绑定后重试") {
      return false;
    } else if (e.detail.errMsg == "getPhoneNumber:fail:cancel to verify sms") {
      return false;
    } else {
      that.getAjaxPhone(iv, encryptedData); //后台解析手机信息
    }
  },

  //后台解析手机号码
  getAjaxPhone(iv, encryptedData) {
    var that = this;
    var openid = wx.getStorageSync('openid');
    app.ajax({
      url: "User/Wx/aesPhoneDecrypt",
      data: {
        iv: iv,
        data: encryptedData,
        openid: openid
      },
      success: function(res) {
        if (res.data.code == 1000) {
          wx.setStorageSync('phone', res.data.data.phoneNumber);
          // app.bindGetnewToken(2, '', 1); //重新调取token
          that.login(res.data.data.phoneNumber, openid);
        } else {
          app.showToast(res.data.message, "none", 2000, function() {});
          return false;
        }
      }
    })
  },

  //登录
  login(phone, openid){
    var that = this;
    app.ajax({
      url: 'User/User/marketingLand',
      method: "POST",
      data: {
        tel: phone,
        bind_type: 2, //绑定类型1验证码绑定，2一键授权绑定
        openid: openid,
      },
      success: function (res) {
        if (res.data.code == -1105) {
          wx.setStorageSync('is_login', false);
          app.redirectLogin();
        }
        wx.setStorageSync('token', res.data.data.token);
        wx.setStorageSync('user_id', res.data.data.user_id);
        app.showToast(res.data.message, "none", 1000, function () { });
        if (res.data.code == 1000) {
          wx.navigateBack({
            delta: 1
          })
        }
      }
    })
  },

  //跳转申请市场推广
  navApply(){
    // wx.navigateToMiniProgram({
    //   appId: 'wx1c239d0d21a9c0fb',
    //   path: 'pages/user/userHome/userHome',
    //   envVersion: 'release',//develop	开发版	trial	体验版	release	正式版
    //   success(res) {
    //     // 打开成功
    //   }
    // })
    wx.navigateTo({
      url: '/pages/distributor/distributorApply/distributorApply',
    })
  },
  //跳转申请查询(申请结果)
  navApplyResult(){
    wx.navigateTo({
      url: '/pages/distributor/distributorApplyResult/distributorApplyResult',
    })
  },
})