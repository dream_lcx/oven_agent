// pages/customService/customService.js
var app = getApp();
var innerAudioContext = wx.createInnerAudioContext();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrollTop: 0,
    information: '', //输入问题
    common_problem: [], // 问题列表结果
    msgList: [{
      msg: '哈喽，我是智能客服~, 您可能需要咨询以下问题:',
      isMine: false,
      msgtype: 1 //是否显示问题列表 1 显示 2 不显示
    }],
    searchList: [], //模糊搜索列表
    searchShow: false, //模糊搜索弹窗
    usermagInfo:[] //用户信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.getCommonProblem(); // 获取问题列表
    that.getUserInfo();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  onReady(res) {
    this.videoContext = wx.createVideoContext('myVideo')
  },

  // 获取问题列表
  getCommonProblem: function(name, type) {
    var that = this;
    var problem_name = name; // 内容搜索
    var is_default = type; // 问题类型 1:用户发起，2:默认问题
    var msgList = that.data.msgList;
    // Ajax请求
    app.ajax({
      url: 'User/Commonproblem/getCommonProblem',
      data: {
        is_default: is_default,
        problem_name: problem_name
      },
      success: function(res) {
        if (res.data.code == 1000) {
          that.setData({
            common_problem: res.data.data
          })
        } else {
          msgList.push({
            msg: '',
            isMine: false,
            msgtype: 1
          })
          that.setData({
            msgList: msgList
          })
        }
      }
    })
  },

  // 获取问题回复答案
  commonProblemDetail: function(e) {
    var that = this;
    var id = e.currentTarget.dataset.id; // 问题ID
    var msgList = that.data.msgList;
    msgList.push({
      msg: e.currentTarget.dataset.title,
      isMine: true,
    })
    that.setData({
      msgList: msgList,
    })
    // Ajax请求
    app.ajax({
      url: 'User/Commonproblem/getCommonProblemDetail',
      data: {
        id: id
      },
      success: function(res) {
        if (res.data.code == 1000) {
          msgList.push({
            msg: res.data.data.reply_content,
            reply_type: res.data.data.reply_type,
            reply_file: res.data.data.reply_file,
            isMine: false,
            msgtype: 2,
            isplay: false
          })
          that.setData({
            msgList: msgList,
            searchShow: false,
            information: ''
          })
        }
        that.bottom();
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },




  //聊天消息始终显示最底端
  bottom: function() {
    var query = wx.createSelectorQuery()
    query.select('#flag').boundingClientRect()
    query.selectViewport().scrollOffset()
    query.exec(function(res) {
      wx.pageScrollTo({
        scrollTop: res[0].bottom // #the-id节点的下边界坐标
      })
      res[1].scrollTop // 显示区域的竖直滚动位置
    })
    // wx.createSelectorQuery().select('#flag').boundingClientRect(function(rect) {
    //   // 使页面滚动到底部
    //   wx.pageScrollTo({
    //     scrollTop: rect.bottom
    //   })
    // }).exec();
  },

  // 输入信息
  inputmsg(e) {
    var that = this;
    that.setData({
      information: e.detail.value
    })
    app.ajax({
      url: 'User/Commonproblem/searchCommonProblem',
      data: {
        is_default: '',
        problem_name: e.detail.value
      },
      success: function(res) {
        if (res.data.code == 1000) {
          that.setData({
            searchList: res.data.data,
            searchShow: true
          })
        } else {
          that.setData({
            searchList: '',
            searchShow: false
          })
        }
      }
    })
  },

  // 发送消息
  send() {
    var that = this;
    var msgList = that.data.msgList;
    if (that.data.information == '') {
      app.showToast('请输入问题');
      return;
    }
    msgList.push({
      msg: that.data.information,
      isMine: true
    })
    that.getCommonProblem(that.data.information, 1);
    that.setData({
      msgList: msgList,
      information: '',
      searchShow: false
    })
    that.bottom();
  },

  // 播放音频
  playAudio(e) {
    var that = this;
    var reply_file = e.currentTarget.dataset.src;
    var isplay = e.currentTarget.dataset.isplay;
    var i = e.currentTarget.dataset.i;
    var msgList = that.data.msgList;
    var play = "msgList[" + i + "].isplay";
    innerAudioContext.src = reply_file;
    innerAudioContext.stop();
    for (var i = 0; i < msgList.length; i++) {
      msgList[i].isplay = false
    }
    that.setData({
      msgList: msgList
    })
    console.log(msgList)
    if (isplay) {
      innerAudioContext.stop();
      that.setData({
        [play]: false
      })
    } else {
      innerAudioContext.play();
      that.setData({
        [play]: true
      })
    }
    // 自然播放结束
    innerAudioContext.onEnded(() => {
      that.setData({
        [play]: false
      })
    })
  },

  // 获取用户信息
  getUserInfo() {
    var that = this;
    app.ajax({
      url: "User/Spread/getInfo",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            usermagInfo: res.data.data
          })
        }
      }
    })
  },

})